﻿<div id="profile">
</div>
<div id="experienceBar">
  <div id="experience"></div>
</div>

<div id="resourcesBar">
  <?php 
    $capacity = 2000 + verifyStorage()*3000;
  ?>
  <ul>
    <li>
      <div id="wood" title="Drevo"></div>
      <span id="resource" title="Počet dreva v sklade">
        <?php showBasicResources('woods') ?>
      </span>
      <span id="income" title="Príjem za hodinu">
        +25
      </span>
      <span id="capacity" title="Kapacita skladu">
        <?php 
        echo $capacity;
        ?>
      </span>
    </li>
    <li>
      <div id="stone" title="Kameň"></div>
      <span id="resource" title="Počet kameňa v sklade">
        <?php showBasicResources('stones') ?>
      </span>
      <span id="income" title="Príjem za hodinu">
        +25
      </span>
      <span id="capacity" title="Kapacita skladu">
        <?php 
        echo $capacity;
        ?>
      </span>
    </li>
    <li>
      <div id="gold" title="Zlato"></div>
      <span id="resource" title="Počet zlata v sklade">
        <?php showBasicResources('golds') ?>
      </span>
      <span id="income" title="Príjem za hodinu">
        +25
      </span>
      <span id="capacity" title="Kapacita skladu">
        <?php 
        echo $capacity;
        ?>
      </span> 
    </li>
    <li>
      <div id="iron" title="Železo"></div>
      <span id="resource" title="Počet železa v sklade">
        <?php showBasicResources('irons') ?>
      </span>
      <span id="income" title="Príjem za hodinu">
        +25
      </span>
      <span id="capacity" title="Kapacita skladu">
        <?php 
        echo $capacity;
        ?>
      </span>
    </li>
    <li>
      <div id="earth" title="hlina"></div>
      <span id="resource" title="Počet hliny v sklade">
        <?php showBasicResources('earths') ?>
      </span>
      <span id="income" title="Príjem za hodinu">
        +25
      </span>
      <span id="capacity" title="Kapacita skladu">
        <?php 
        echo $capacity;
        ?>
      </span>
    </li>
  </ul>
</div>


<div id="score_table">
  <span class="pointer" title="Zavrieť okno s hodnotením">
    X
  </span>
  <h1>
    Prehľad hodnotenia hráčov
  </h1>
  <div id="table">
    <table>
      <thead>
        <th>
          <span title="Mená hráčov">
            Hráč
          </span>
        </th>
        <th>
          <span title="Skúsenosti hráčov">
            Skúsenosti
          </span>
        </th>
      </thead>
      <tbody>
        <?php
          score_table("username");
        ?>
      </tbody>
    </table>
  </div>
</div>

<div id="buildPlace1" class="buildplace"><a href="?place=1#build_place"> <?php showBuilding('placeOne'); ?></a></div>
<div id="buildPlace2" class="buildplace"><a href="?place=2#build_place"><?php showBuilding('placeTwo'); ?></a></div>
<div id="buildPlace3" class="buildplace"><a href="?place=3#build_place"> <?php showBuilding('placeThree'); ?></a></div>
<div id="buildPlace4" class="buildplace"><a href="?place=4#build_place"><?php showBuilding('placeFour'); ?></a></div>
<div id="buildPlace5" class="buildplace"><a href="?place=5#build_place"> <?php showBuilding('placeFive'); ?></a></div>
<div id="buildPlace6" class="buildplace"><a href="?place=6#build_place"><?php showBuilding('placeSix'); ?></a></div>
<div id="buildPlace7" class="buildplace"><a href="?place=7#build_place"> <?php showBuilding('placeSeven'); ?></a></div>
<div id="buildPlace8" class="buildplace"><a href="?place=8#build_place"><?php showBuilding('placeEight'); ?></a></div>
<div id="buildPlace9" class="buildplace"><a href="?place=9#build_place"> <?php showBuilding('placeNine'); ?></a></div>
<div id="buildPlace10" class="buildplace"><a href="?place=10#build_place"><?php showBuilding('placeTen'); ?></a></div>
<div id="logOut">
</div>

<div id="actionBar">
  <ul>
    <li id="map">
      <a href="#world" title="Náhľad sveta"></a>
    </li>
    <li id="town">
      <a href="#town" title="Náhľad mesta"></a>
    </li>
    <li id="diplomacy">
      <a href="#diplomacy" title="Diplomacia"></a>
    </li>
    <li id="stoarm">
      <a href="#armory" title="Sklad/Zbrojnica"></a>
    </li>
    <li id="army">
      <a href="#army" title="Armáda"></a>
    </li>
    <li id="administrator">
      <a href="#administrator" title="Správca mesta"></a>
    </li>
    <li id="research">
      <a href="#research" title="Výskum"></a>
    </li>
    <li id="statistic">
      <a href="#statistic" title="Hodnotenia"></a>
    </li>
    <li id="help">
      <a href="#help" title="Pomoc"></a>
    </li>
</div>

<div id="experienceBar">
  <div id="showExp" title="<?php echo returnExp(); ?> /1000 skúseností">
  <?php $expWidth = (290/1000)*returnExp(); ?>
    <img src="images/exp.jpg" alt="<?php echo returnExp(); ?> /1000 skúseností" title="<?php echo returnExp(); ?> /1000 skúseností" width="<?php echo $expWidth;?>px" height="5px"> </div>
</div>

<div id="playerProfile">
<?php
  showPlayerProfile();
?>
</div>

<div id="bottomBar">
  <span title="Zavrieť aktuálny výber" class="pointer">
    x
  </span>
  <span title="Schovať panel" class="pointer">
    «
  </span>
  <h1></h1>
  <div id="buildings">
  <?php 
    showBuildings(1);
  ?>
  </div>
  <div id="troops">
    <?php
      showTroops();
    ?>
  </div>
</div>
<div id="infoBar">
  <span title="Zavrieť aktuálny výber" class="pointer">
    x
  </span>
  <div id="infoBuilding">
    <div id="infoTownHall">
      <?php 
        infoBuilding("Hlavná budova", 1);
      ?>
    </div>
    <div id="infoStorage">
      <?php 
        infoBuilding("Sklad", 1);
      ?>
    </div>
    <div id="infoArmory">
      <?php 
        infoBuilding("Zbrojnica", 1);
      ?>
    </div>
    <div id="infoBarrack">
      <?php 
        infoBuilding("Kasárne", 1);
      ?>
    </div>
    <div id="infoBurrow">
      <?php 
        infoBuilding("Nora", 1);
      ?>
    </div>
    <div id="infoPlatner">
      <?php 
        infoBuilding("Dielne", 1);
      ?>
    </div>
  </div>
  <div id="infoUnit">
    <div id="infoArcher">
      <?php
        infoUnit('Lukostrelec');
      ?>
    </div>
    <div id="infoSlinger">
      <?php
        infoUnit('Bojovník s prakom');
      ?>
    </div>
    <div id="infoSpearman">
      <?php
        infoUnit('Kopijník');
      ?>
    </div>
    <div id="infoSwordman">
      <?php
        infoUnit('Tažkoodenec');
      ?>
    </div>
  </div>
</div>

<form method="post" id="logoutButton" class="logout"> 
  <input name="logout" type="submit" value="Odhlásiť"> 
</form>
