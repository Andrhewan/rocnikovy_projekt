﻿<?php 
  if(isset($_POST['saveNation']) && isset($_POST['nation'])) {
    save_settings('id_nation',$_POST['nation']);
  }
  if(isset($_POST['saveLand']) && isset($_POST['region'])) {
    save_settings('id_genus',$_POST['region']);
  }
  if(isset($_POST['saveTown']) && isset($_POST['nameTown'])) {
    save_settings('capital',$_POST['nameTown']);
  }
  if(no_complete_setting('id_nation')) {
?>
<section>
  <div id="selectNation">
    <h1>Goblini</h1>
    <form method="post">
      <ul>
      <?php 
        listNations('g');
      ?>
      </ul>
      <input type="submit" name="saveNation" value="Vybrať">
      <h1>Trollovia</h1>
      <ul>
      <?php 
        listNations('t');
      ?>
      </ul>
      <input type="submit" name="saveNation" value="Vybrať">
    </form>
  </div>
<?php 
  }
  elseif (no_complete_setting('id_genus')) {
?>
  
  <div id="selectLand">
    <form method="post">
      <label for="reg">Vyber si oblast:
      </label>  
      <select name="region">    
        <?php 
        select_region();
        ?> 
      </select>
      <br>  
      <input name="saveLand" type="submit" value="Vybrať">
     </form>  
   </div>
<?php 
  }
  elseif(no_complete_setting('capital')) {
?>
   
   <div id="selectTown">  
    <form method="post">
      <label for="nameTown">Nazov mesta:
      </label>
      <input type="text" maxlength="20" name="nameTown" size="20" required="required" id="nameTown" value="">  
      <br>  
      <input name="saveTown" type="submit" value="Uložiť názov">
    </form>
  </div>
</section>
<?php
  }
  else {
    include('onlineGame.php');
  }
?>
<form method="post" id="logoutButton"> 
  <input name="logout" type="submit" value="Odhlásiť"> 
</form>