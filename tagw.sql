-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Hostiteľ: 127.0.0.1
-- Vygenerované: Po 21.Apr 2014, 23:55
-- Verzia serveru: 5.5.27
-- Verzia PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáza: `tagw`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `armory`
--

CREATE TABLE IF NOT EXISTS `armory` (
  `id` int(11) NOT NULL,
  `bows` int(11) NOT NULL,
  `spears` int(11) NOT NULL,
  `light_armours` int(11) NOT NULL,
  `heavy_armours` int(11) NOT NULL,
  `swords` int(11) NOT NULL,
  `slings` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `armory`
--

INSERT INTO `armory` (`id`, `bows`, `spears`, `light_armours`, `heavy_armours`, `swords`, `slings`) VALUES
(40, 0, 0, 0, 0, 0, 0),
(41, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `buildings`
--

CREATE TABLE IF NOT EXISTS `buildings` (
  `idb` int(11) NOT NULL AUTO_INCREMENT,
  `building` text COLLATE utf8_slovak_ci NOT NULL,
  `level` int(11) NOT NULL,
  `wood` int(11) NOT NULL,
  `stone` int(11) NOT NULL,
  `gold` int(11) NOT NULL,
  `iron` int(11) NOT NULL,
  `earth` int(11) NOT NULL,
  `population` int(11) NOT NULL,
  `store` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `exps` int(11) NOT NULL,
  `time` text COLLATE utf8_slovak_ci NOT NULL,
  `description` text COLLATE utf8_slovak_ci NOT NULL,
  `post` text COLLATE utf8_slovak_ci NOT NULL,
  `charnat` varchar(11) COLLATE utf8_slovak_ci NOT NULL,
  PRIMARY KEY (`idb`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci AUTO_INCREMENT=38 ;

--
-- Sťahujem dáta pre tabuľku `buildings`
--

INSERT INTO `buildings` (`idb`, `building`, `level`, `wood`, `stone`, `gold`, `iron`, `earth`, `population`, `store`, `score`, `exps`, `time`, `description`, `post`, `charnat`) VALUES
(1, 'Hlavná budova', 1, 400, 400, 500, 100, 100, 25, 0, 250, 50, '1:45', 'Hlavná budova je základom každej usadlosti. Z tohto miesta môžeš riadiť svoje mesto a mať prehľad o všetkom. ', 'buildTownHall', 'g'),
(2, 'Kasárne', 1, 250, 300, 150, 50, 75, 0, 0, 100, 25, '1:00', 'V kasárňach môžeš regrutovať vojakov. ', 'buildBarrack', 'g'),
(3, 'Nora', 1, 180, 50, 50, 10, 50, 5, 0, 10, 15, '1:00', 'V norách žijú tvoji obyvatelia. Čím vyššia úroveň nory, tým viac potrebných obyvateľov môžeš mať.', 'buildBurrow', 'g'),
(4, 'Sklad', 1, 200, 250, 150, 50, 50, 0, 3000, 50, 20, '1:15', 'V skladisku môžeš skladovať suroviny. Vyššou úrovňou skladu môžeš uskladniť viacej surovín.', 'buildStorage', 'g'),
(5, 'Dielne', 1, 150, 150, 150, 300, 50, 0, 0, 50, 25, '3:00', 'V dielni môžeš vyrábať brnenia pre svoje vojská. Čím vyššia úroveň dielne, tým odolnejšie brnenie môžeš mať.', 'buildArmoryWork', 'g'),
(6, 'Hlavná budova', 2, 1000, 1000, 1200, 750, 0, 100, 0, 300, 0, '', 'Hlavná budova je základom každej usadlosti. Z tohto miesta môžeš riadiť svoje mesto a mať prehľad o všetkom. ', '', 'g'),
(7, 'Hlavná budova', 3, 2000, 2000, 2300, 2000, 0, 250, 0, 750, 0, '', 'Hlavná budova je základom každej usadlosti. Z tohto miesta môžeš riadiť svoje mesto a mať prehľad o všetkom. ', '', 'g'),
(8, 'Hlavná budova', 4, 3000, 3000, 3250, 2100, 0, 500, 0, 300, 0, '', 'Hlavná budova je základom každej usadlosti. Z tohto miesta môžeš riadiť svoje mesto a mať prehľad o všetkom. ', '', 'g'),
(9, 'Kasárne', 2, 350, 400, 200, 100, 0, 0, 0, 150, 0, '', 'V kasárňach môžeš regrutovať vojakov. ', '', 'g'),
(10, 'Kasárne', 3, 450, 600, 400, 200, 0, 0, 0, 175, 0, '', 'V kasárňach môžeš regrutovať vojakov. ', '', 'g'),
(11, 'Kasárne', 4, 600, 750, 300, 250, 0, 0, 0, 200, 0, '', 'V kasárňach môžeš regrutovať vojakov. ', '', 'g'),
(12, 'Dielňa na výrobu brnenia', 2, 200, 200, 200, 400, 0, 0, 0, 75, 0, '', 'V dielni môžeš vyrábať brnenia pre svoje vojská. Čím vyššia úroveň dielne, tým odolnejšie brnenie môžeš mať.', '', 'g'),
(13, 'Dielňa na výrobu brnenia', 3, 300, 300, 300, 600, 0, 0, 0, 100, 0, '', 'V dielni môžeš vyrábať brnenia pre svoje vojská. Čím vyššia úroveň dielne, tým odolnejšie brnenie môžeš mať.', '', 'g'),
(14, 'Sklad', 2, 250, 300, 150, 100, 0, 0, 6000, 75, 0, '', 'V skladisku môžeš skladovať suroviny. Vyššou úrovňou skladu môžeš uskladniť viacej surovín.', '', 'g'),
(15, 'Sklad', 3, 300, 350, 200, 150, 0, 0, 11000, 100, 0, '', 'V skladisku môžeš skladovať suroviny. Vyššou úrovňou skladu môžeš uskladniť viacej surovín.', '', 'g'),
(16, 'Sklad', 4, 400, 500, 350, 200, 0, 0, 20000, 125, 0, '', 'V skladisku môžeš skladovať suroviny. Vyššou úrovňou skladu môžeš uskladniť viacej surovín.', '', 'g'),
(17, 'Sklad', 5, 500, 650, 400, 300, 0, 0, 30000, 150, 0, '', 'V skladisku môžeš skladovať suroviny. Vyššou úrovňou skladu môžeš uskladniť viacej surovín.', '', 'g'),
(18, 'Sklad', 6, 650, 800, 600, 400, 0, 0, 40000, 200, 0, '', 'V skladisku môžeš skladovať suroviny. Vyššou úrovňou skladu môžeš uskladniť viacej surovín.', '', 'g'),
(19, 'Nora', 2, 200, 75, 75, 25, 0, 15, 0, 20, 0, '', 'V norách žijú tvoji obyvatelia. Čím vyššia úroveň nory, tým viac potrebných obyvateľov môžeš mať.', '', 'g'),
(20, 'Nora', 3, 225, 100, 100, 50, 0, 25, 0, 25, 0, '', 'V norách žijú tvoji obyvatelia. Čím vyššia úroveň nory, tým viac potrebných obyvateľov môžeš mať.', '', 'g'),
(21, 'Nora', 4, 250, 150, 150, 75, 0, 35, 0, 30, 0, '', 'V norách žijú tvoji obyvatelia. Čím vyššia úroveň nory, tým viac potrebných obyvateľov môžeš mať.', '', 'g'),
(22, 'Nora', 5, 275, 175, 175, 100, 0, 45, 0, 35, 0, '', 'V norách žijú tvoji obyvatelia. Čím vyššia úroveň nory, tým viac potrebných obyvateľov môžeš mať.', '', 'g'),
(23, 'Nora', 6, 300, 200, 200, 125, 0, 55, 0, 40, 0, '', 'V norách žijú tvoji obyvatelia. Čím vyššia úroveň nory, tým viac potrebných obyvateľov môžeš mať.', '', 'g'),
(24, 'Nora', 7, 350, 300, 250, 150, 0, 65, 0, 50, 0, '', 'V norách žijú tvoji obyvatelia. Čím vyššia úroveň nory, tým viac potrebných obyvateľov môžeš mať.', '', 'g'),
(25, 'Nora', 8, 425, 350, 300, 175, 0, 75, 0, 60, 0, '', 'V norách žijú tvoji obyvatelia. Čím vyššia úroveň nory, tým viac potrebných obyvateľov môžeš mať.', '', 'g'),
(26, 'Nora', 9, 550, 475, 400, 200, 0, 85, 0, 70, 0, '', 'V norách žijú tvoji obyvatelia. Čím vyššia úroveň nory, tým viac potrebných obyvateľov môžeš mať.', '', 'g'),
(27, 'Nora', 10, 700, 600, 550, 225, 0, 100, 0, 80, 0, '', 'V norách žijú tvoji obyvatelia. Čím vyššia úroveň nory, tým viac potrebných obyvateľov môžeš mať.', '', 'g'),
(28, 'Zbrojnica', 1, 250, 250, 150, 250, 0, 0, 0, 25, 15, '1:00', 'V zbrojnici skladuješ zbrane pre tvoje vojsko.', 'buildArmory', 'g'),
(29, 'Zbrojnica', 2, 300, 300, 200, 300, 0, 0, 0, 35, 0, '', 'V zbrojnici skladuješ zbrane pre tvoje vojsko.', '', 'g'),
(30, 'Zbrojnica', 3, 350, 350, 250, 350, 0, 0, 0, 45, 0, '', 'V zbrojnici skladuješ zbrane pre tvoje vojsko.', '', 'g'),
(31, 'Dielňa na výrobu strelných zbraní', 1, 250, 100, 150, 50, 0, 0, 0, 50, 0, '', 'V dielni môžeš vyrábať strelné zbrane pre svoje vojská.', '', '0'),
(32, 'Dielňa na výrobu strelných zbraní', 2, 300, 200, 250, 100, 0, 0, 0, 60, 0, '', 'V dielni môžeš vyrábať strelné zbrane pre svoje vojská.', '', '0'),
(33, 'Kruhový oltár', 1, 100, 500, 250, 500, 0, 0, 0, 50, 0, '', 'Kruhový oltár zvyšuje náboženskú silu a vplyv v tvojom meste. Je dôležité postaviť kruhový oltár v usadlosti, aby si mohol stavať ďalšie miesta k rozvoju kultúry a náboženstva.', '', '0'),
(34, 'Strom vedomia', 1, 500, 150, 100, 0, 0, 0, 0, 50, 0, '', 'Strom vedomia je miesto, kde prebiehajú obrady, stretávajú sa šamani a mudrci z celého okolia tvojej usadlosti. Je to jedinečná budova iba pre goblinov.', '', '0'),
(35, 'Strom vedomia', 2, 600, 200, 150, 0, 0, 0, 0, 100, 0, '', 'Strom vedomia je miesto, kde prebiehajú obrady, stretávajú sa šamani a mudrci z celého okolia tvojej usadlosti. Je to jedinečná budova iba pre goblinov.', '', '0'),
(36, 'Strom vedomia', 3, 700, 250, 250, 0, 0, 0, 0, 150, 0, '', 'Strom vedomia je miesto, kde prebiehajú obrady, stretávajú sa šamani a mudrci z celého okolia tvojej usadlosti. Je to jedinečná budova iba pre goblinov.', '', '0'),
(37, 'Námestie', 1, 300, 300, 300, 300, 0, 50, 0, 35, 0, '', 'Námestie je miesto, kde sa stretávajú všetci obyvatelia tvojej usadlosti. Taktiež poskytuje miesto pre nových obyvateľov.', '', '0');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `buildplace`
--

CREATE TABLE IF NOT EXISTS `buildplace` (
  `id` int(11) NOT NULL,
  `1` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `2` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `3` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `4` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `5` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `6` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `7` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `8` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `9` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `10` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `11` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `12` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `13` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `14` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `15` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `16` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `17` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `buildplace`
--

INSERT INTO `buildplace` (`id`, `1`, `2`, `3`, `4`, `5`, `6`, `7`, `8`, `9`, `10`, `11`, `12`, `13`, `14`, `15`, `16`, `17`) VALUES
(40, 'Kasárne', 'Zbrojnica', 'Kasárne', 'Dielne', 'Kasárne', 'Sklad', 'Zbrojnica', 'Nora', 'Nora', 'Zbrojnica', 'Kasárne', '', '', 'Kasárne', 'Hlavná budova', 'Nora', 'Kasárne'),
(41, 'Sklad', '', 'Sklad', '', 'Nora', '', '', '', '', '', '', '', '', '', '', '', ''),
(43, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `experience`
--

CREATE TABLE IF NOT EXISTS `experience` (
  `id` int(11) NOT NULL,
  `exp` int(11) NOT NULL,
  `score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `experience`
--

INSERT INTO `experience` (`id`, `exp`, `score`) VALUES
(40, 90, 300),
(41, 0, 0),
(42, 0, 0),
(43, 0, 0);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `genus_information`
--

CREATE TABLE IF NOT EXISTS `genus_information` (
  `id_gen` int(11) NOT NULL AUTO_INCREMENT,
  `id_nation` int(11) NOT NULL,
  `genus` varchar(100) COLLATE utf8_slovak_ci NOT NULL,
  `local_main_settlement` varchar(100) COLLATE utf8_slovak_ci NOT NULL,
  PRIMARY KEY (`id_gen`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci AUTO_INCREMENT=11 ;

--
-- Sťahujem dáta pre tabuľku `genus_information`
--

INSERT INTO `genus_information` (`id_gen`, `id_nation`, `genus`, `local_main_settlement`) VALUES
(1, 1, 'Hikvuhij', 'Hijniitui'),
(2, 1, 'Hahkvui', 'Jiihve'),
(3, 2, 'Khhaj', 'Tuinii'),
(4, 2, 'Ghuuniifh', 'Kvuphiurii'),
(5, 3, 'Kvikhje', 'Kvohahnii'),
(6, 3, 'Hikhve', 'Ahhghuujii'),
(7, 4, 'Ahhkhvojii', 'Riiniijii'),
(8, 4, 'Huhhjheenii', 'Huhniijii'),
(9, 5, 'Heewhuhhchrii', 'Khhohhph'),
(10, 5, 'Ahhnojii', 'Kvejhee');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `landbuildplace`
--

CREATE TABLE IF NOT EXISTS `landbuildplace` (
  `id` int(11) NOT NULL,
  `1` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `2` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `3` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `4` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `5` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `landbuildplace`
--

INSERT INTO `landbuildplace` (`id`, `1`, `2`, `3`, `4`, `5`) VALUES
(40, '', '', '', '', ''),
(41, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `nations`
--

CREATE TABLE IF NOT EXISTS `nations` (
  `id_nation` int(11) NOT NULL AUTO_INCREMENT,
  `charnat` varchar(1) COLLATE utf8_slovak_ci NOT NULL,
  `nations` varchar(100) COLLATE utf8_slovak_ci NOT NULL,
  PRIMARY KEY (`id_nation`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci AUTO_INCREMENT=6 ;

--
-- Sťahujem dáta pre tabuľku `nations`
--

INSERT INTO `nations` (`id_nation`, `charnat`, `nations`) VALUES
(1, 'g', 'Lesní goblini'),
(2, 'g', 'Severní goblini'),
(3, 'g', 'Horskí goblini'),
(4, 'g', 'Goblini západných močiarov'),
(5, 'g', 'Podzemní goblini');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `player_data`
--

CREATE TABLE IF NOT EXISTS `player_data` (
  `id` int(11) NOT NULL,
  `id_nation` int(11) NOT NULL,
  `id_genus` int(11) NOT NULL,
  `capital` text NOT NULL,
  `activity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `player_data`
--

INSERT INTO `player_data` (`id`, `id_nation`, `id_genus`, `capital`, `activity`) VALUES
(40, 1, 2, 'Moje mesto', '2014-04-07 17:09:41'),
(41, 2, 3, 'Matkino mesto', '2014-04-07 17:15:26'),
(42, 3, 6, 'LOlovo', '2014-04-07 19:40:48'),
(43, 4, 8, 'Kikak mesto', '2014-04-09 12:05:42');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `player_units`
--

CREATE TABLE IF NOT EXISTS `player_units` (
  `id` int(11) NOT NULL,
  `slingers` int(11) NOT NULL,
  `archers` int(11) NOT NULL,
  `swordmen` int(11) NOT NULL,
  `spearmen` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

--
-- Sťahujem dáta pre tabuľku `player_units`
--

INSERT INTO `player_units` (`id`, `slingers`, `archers`, `swordmen`, `spearmen`) VALUES
(1, 0, 0, 0, 0),
(2, 0, 10, 0, 0);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `resources_data`
--

CREATE TABLE IF NOT EXISTS `resources_data` (
  `id` int(11) NOT NULL,
  `wood` int(11) NOT NULL,
  `stone` int(11) NOT NULL,
  `gold` int(11) NOT NULL,
  `earth` int(11) NOT NULL,
  `iron` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `resources_data`
--

INSERT INTO `resources_data` (`id`, `wood`, `stone`, `gold`, `earth`, `iron`) VALUES
(40, 15075, 17400, 18400, 1450, 19600),
(41, 129, 209, 309, 409, 449),
(42, 509, 509, 509, 509, 509),
(43, 509, 509, 509, 509, 509);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `troops`
--

CREATE TABLE IF NOT EXISTS `troops` (
  `idt` int(11) NOT NULL,
  `unit` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `bow` int(11) NOT NULL,
  `sling` int(11) NOT NULL,
  `spear` int(11) NOT NULL,
  `sword` int(11) NOT NULL,
  `light_armour` int(11) NOT NULL,
  `heavy_armour` int(11) NOT NULL,
  `gold` int(11) NOT NULL,
  `time` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `post` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `charnat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `troops`
--

INSERT INTO `troops` (`idt`, `unit`, `bow`, `sling`, `spear`, `sword`, `light_armour`, `heavy_armour`, `gold`, `time`, `description`, `post`, `charnat`) VALUES
(1, 'Lukostrelec', 1, 0, 0, 0, 1, 0, 100, '0:25', 'Lukostrelci sú jednotky, ktoré dokáže z veľkej vzdialenosti účinne ničiť nepriateľské ciele. ', 'recArcher', 'g'),
(2, 'Bojovník s prakom', 0, 1, 0, 0, 1, 0, 30, '0:20', 'Bojovník s prakom je ľahká jednotka, ktorá je vhodná na ľahko obrnené ciele.', 'recSlinger', 'g'),
(3, 'Kopijník', 0, 0, 1, 0, 0, 1, 300, '0:50', 'Kopijníci sú ťažká jednotka vhodná na udržiavanie formácie na bojovej línii. Dlhé kópie prepichnú nepriateľa skôr ako sa k nim priblíži.', 'recSpearman', 'g'),
(4, 'Tažkoodenec', 0, 0, 0, 1, 0, 1, 390, '1:05', 'Ťažká bojová jednotka, ktorá je základom každej armády. Húževnatí bojovníci sú skoro nezraniteľní v ich ťažkom brnení a mečmi dokážu rozseknúť nejednu hlavu.', 'recSwordman', 'g');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `user_data`
--

CREATE TABLE IF NOT EXISTS `user_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `userpassword` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uID` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Sťahujem dáta pre tabuľku `user_data`
--

INSERT INTO `user_data` (`id`, `username`, `userpassword`) VALUES
(40, 'Vlado', '66d30c93619758b4fd14eb88a7c7b9ff1d4c4b6e'),
(41, 'Matka', '337ba687eae06694ceaa4a50c2050cc29ae64981'),
(42, 'Lolko', 'c6de6bb9ef5ad2eadaf87b56784b8e190622d0d0'),
(43, 'Kika', '7aec1362e6c948bf33d23d5ce96b7d51d919c58d');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
