﻿<?php
function head($title) {
?>
<!DOCTYPE HTML>
<html>           
  <head>                 
    <meta charset="utf-8">              
    <title>                    
      <?php echo $title; ?>                
    </title>
    <link type="text/css" rel="stylesheet" href="css/cssStyles.php">
    <link type="text/css" href="css/css.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/scripts.js"></script>                 
  </head>           
  <body>
<?php
}

function verifyInformation($name) {
  $name = addslashes(strip_tags(trim($name)));
  return (strlen($name) >= 3) && (strlen($name) <= 50);
}
?>