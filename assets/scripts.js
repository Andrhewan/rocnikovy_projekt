// schova vsetky elementy div
function hideDiv($localization) {
  $localization.find('div').each(function(){
    $(this).hide()
  });
}

// zobrazuje aktualny stav surovin z db
function show_updt_wood(){
  $.get("main/returnWood", function(data){
    $('.wood').text(data);
  });
}

function show_updt_stone(){
  $.get("main/returnStone", function(data){
    $('.stone').text(data);
  });
}

function show_updt_gold(){
  $.get("main/returnGold", function(data){
    $('.gold').text(data);
  });
}

function show_updt_earth(){
  $.get("main/returnEarth", function(data){
    $('.earth').text(data);
  });
}

function show_updt_iron(){
  $.get("main/returnIron", function(data){
    $('.iron').text(data);
  });
}

// automatic update resources
function show_updt(){
  $.get("main/returnWood", function(data){
    $('.wood').text(data);
  });
 
  $.get("main/returnStone", function(data){
    $('.stone').text(data);
  });
 
  $.get("main/returnGold", function(data){
    $('.gold').text(data);
  });
 
  $.get("main/returnEarth", function(data){
    $('.earth').text(data);
  });
}

function automatic_updating(){
    setInterval(function(){
      show_updt();
      $.post("main/update");
    },144000);
}

// menu stavebneho miesta
// stavanie budov na stavebnych miestach
function buildIndex(id){
  $('#buildBurrow').click(function() {
    $.post("main/verifyBuilding", {building : 'Nora', placeid : id},function(data){
      if(data == true){
        timingBuilding(0,12);
      }
    });
  });
  $('#buildBarrack').on('click',function(){
    $.post("main/build", {building : 'Kasárne', placeid : id});
  });
  $('#buildTownHall').on('click',function(){
    $.post("main/build", {building : 'Hlavná budova', placeid : id});
  });
  $('#buildStorage').on('click',function(){
    $.post("main/build", {building : 'Sklad', placeid : id});
    $.post("main/updateStorageCapacity", {capacity: 2000});
  });
  $('#buildArmoryWork').on('click',function(){
    $.post("main/build", {building : 'Dielne', placeid : id});
  });
  $('#buildArmory').on('click',function(){
    $.post("main/build", {building : 'Zbrojnica', placeid : id});
  });
}

// pridanie tlacidiel na pracovanie so stavebnym miestom
function buildplaceMenu(){
  $('#places').find('.buildPlace').each(function(){
    // identifikacia budovy na stavebnom mieste
    var buildingName = $(this).data('buildingname');
    $(this).hover(function(){
      if(buildingName != "Voľné stavebné políčko"){
        $data = '<div id="rebuild" title="Prestavať budovu"></div><div id="upgradeBuilding" title="Vylepšiť budovu na úroveň"></div><h3 class="titleBuilding">'+buildingName+'</h3>';
      }
      else {
        $data = '<div id="rebuild" title="Prestavať budovu"></div><h3 class="titleBuilding">'+buildingName+'</h3>';
      }
      $(this).append($data);
      $(this).find('#rebuild').click(function(){
        hideDiv($('#bottomBar'));
        $('#bottomBar').find('#buildings').show();
        $('#bottomBar').find('#buildings').find('div').each(function(){
          $(this).show();
        });
        $('#bottomBar').find('h1').text('Budovy');
        var idPlace = $(this).parent().data('placeid');
        buildIndex(idPlace);         
      });
    });
    $(this).mouseleave(function(){
      $(this).find('div').each(function(){
        $(this).remove();
      });
      $(this).find('h3').each(function(){
        $(this).remove();
      });
    });
    $(this).on('click', this, function(e){
      if( e.target != this ) {
        return;
      }
      hideDiv($('#infoBar')); 
      $('#infoBar').show();
      $('#infoBar').find('#infoBuilding').show();
      if(buildingName == 'Sklad') {
        $('#infoBar').find('#infoStorage').show();
        $('#infoBar').find('#infoStorage').find('button').hide();
        $('#infoBar').find('#infoStorage').find('table').hide();
      }
      if(buildingName == 'Nora') {
        $('#infoBar').find('#infoBurrow').show();
        $('#infoBar').find('#infoBurrow').find('button').hide();
        $('#infoBar').find('#infoBurrow').find('table').hide();
      }
      if(buildingName == 'Dielne') {
        $('#infoBar').find('#infoArmoryWork').show();
        $('#infoBar').find('#infoArmoryWork').find('button').hide();
        $('#infoBar').find('#infoArmoryWork').find('table').hide();
      }
      if(buildingName == 'Zbrojnica') {
        $('#infoBar').find('#infoArmory').show();
        $('#infoBar').find('#infoArmory').find('div').hide();
        $('#infoBar').find('#infoArmory').append('<div>');
        $.post("main/return_weapons", function(data){
          $obj = JSON.parse(data);
          $('#infoBar').find('#infoArmory').find('div').append('<ul><li>Luk: '+$obj.bows+'</li><li>Prak: '+$obj.slings+'</li><li>Kopije: '+$obj.spears+'</li><li>Meče: '+$obj.swords+'</li><li>Ľahké brnenie: '+$obj.light_armours+'</li><li>Ťažké brnenie: '+$obj.heavy_armours+'</li></ul>');
        });
        $('#infoBar').find('#infoArmory').append('</div>');
        $('#infoBar').find('#infoArmory').find('button').hide();
        $('#infoBar').find('#infoArmory').find('table').hide();
      }
      if(buildingName == 'Kasárne') {
        $('#infoBar').find('#infoBarrack').show();
        $('#infoBar').find('#infoBarrack').find('div').hide();
        $('#infoBar').find('#infoBarrack').append('<div>');
        $.post("main/soldiers", function(data){
          $('#infoBar').find('#infoBarrack').find('div').append('Počet vojakov v usadlosti je ',data);
        });
        $.post("main/troops", function(data){
          $obj = JSON.parse(data);
          $('#infoBar').find('#infoBarrack').find('div').append('<ul><li>Lukostrelec: '+$obj.archers+'</li><li>Bojovník s prakom: '+$obj.slingers+'</li><li>Kopijník: '+$obj.spearmen+'</li><li>Ťažkoodenec: '+$obj.swordmen+'</li></ul>');
        });
        $('#infoBar').find('#infoBarrack').append('</div>');
        $('#infoBar').find('#infoBarrack').find('button').hide();
        $('#infoBar').find('#infoBarrack').find('table').hide();
      }
      if(buildingName == 'Hlavná budova') {
        $('#infoBar').find('#infoTownHall').show();
        $('#infoBar').find('#infoTownHall').find('button').hide();
        $('#infoBar').find('#infoTownHall').find('table').hide();
      }
    });
  });
}

// vytvorenie stavebnych miest
function createPlaces(){
  $.get('main/buildPlaces',function(data){
    $('#places').append(data);
    buildplaceMenu();
  });
}

//vytvorenie stavebnych miest pre landmode
function createLandPlaces(){
  $.get('main/buildLandPlaces',function(data){
    $('#landplaces').append(data);
    $('#landplaces').find('.buildLandPlace').each(function(){
      $(this).on('click',function(){
        $('#bottomBar').find('div').each(function(){
        $(this).hide()
        });
        $('#bottomBar').find('#nothing').show();     
      });
    });
  });
}

function hideElementAtStart() {
  $('#scoreTable').hide();
  $('#helpTable').hide();
  hideDiv($('#bottomBar'));
  $('#infoBar').hide();
}

// nastavenie tabuliek hodnotenia a pomoci
function setTables($table) {
  $table.css({"left":"25%", "top":"25%"});
  $table.show();
  $table.draggable();
}

function actionBarElementsFunction() {
  var h1 = $('#bottomBar').find('h1');
  $('#actionBar').find('#statistic').on('click', function(){
    setTables($('#scoreTable'));
  });

  $('#actionBar').find('#help').on('click', function(){
     setTables($('#helpTable'));
  });

  $('#actionBar').find('#army').on('click',function(){
    hideDiv($('#bottomBar'));
    $('#bottomBar').find('#troops').find('div').each(function(){
      $(this).show();
    });
    $('#bottomBar').find('#troops').show();
    h1.text('Jednotky');
  });
}

// zobrazuje infoBar
function iBShow($localization) {
  $('#infoBar').show();
  $('#infoBar').find($localization).show();
}

// zobrazuje info o budovach
function infoBarBuildings(){
  $('#bottomBar').find('#TownHall').on('click',function(){
    hideDiv($('#infoBar'));
    iBShow('#infoBuilding');
    $('#infoBar').find('#infoTownHall').show();
    $('#infoBar').find('#infoTownHall').find('button').show();
    $('#infoBar').find('#infoTownHall').find('table').show();
  });

  $('#bottomBar').find('#Barrack').on('click',function(){
    hideDiv($('#infoBar'));
    iBShow('#infoBuilding');
    $('#infoBar').find('#infoBarrack').show();
    $('#infoBar').find('#infoBarrack').find('button').show();
    $('#infoBar').find('#infoBarrack').find('table').show();
  });

  $('#bottomBar').find('#Burrow').on('click',function(){
    hideDiv($('#infoBar'));
    iBShow('#infoBuilding');
    $('#infoBar').find('#infoBurrow').show();
    $('#infoBar').find('#infoBurrow').find('button').show();
    $('#infoBar').find('#infoBurrow').find('table').show();
  });

  $('#bottomBar').find('#Storage').on('click',function(){
    hideDiv($('#infoBar'));
    iBShow('#infoBuilding');
    $('#infoBar').find('#infoStorage').show();
    $('#infoBar').find('#infoStorage').find('button').show();
    $('#infoBar').find('#infoStorage').find('table').show();
  });

  $('#bottomBar').find('#Armory').on('click',function(){
    hideDiv($('#infoBar'));
    iBShow('#infoBuilding');
    $('#infoBar').find('#infoArmory').show();
    $('#infoBar').find('#infoArmory').find('button').show();
    $('#infoBar').find('#infoArmory').find('table').show();
    $('#infoBar').find('#infoArmory').find('ul').remove();
  });

  $('#bottomBar').find('#ArmoryWork').on('click',function(){
    hideDiv($('#infoBar'));
    iBShow('#infoBuilding');
    $('#infoBar').find('#infoArmoryWork').show();
    $('#infoBar').find('#infoArmoryWork').find('button').show();
    $('#infoBar').find('#infoArmoryWork').find('table').show();
  });
}

// zobrazuje info o jednotkach
function infoTroopsBar() {
  $('#bottomBar').find('#Archer').on('click',function(){
    hideDiv($('#infoBar'));
    iBShow('#infoUnit');
    $('#infoBar').find('#infoArcher').show();
  });

  $('#bottomBar').find('#Swordman').on('click',function(){
    hideDiv($('#infoBar'));
    iBShow('#infoUnit');
    $('#infoBar').find('#infoSwordman').show();
  });

  $('#bottomBar').find('#Slinger').on('click',function(){
    hideDiv($('#infoBar'));
    iBShow('#infoUnit');
    $('#infoBar').find('#infoSlinger').show();
  });

  $('#bottomBar').find('#Spearman').on('click',function(){
    hideDiv($('#infoBar'));
    iBShow('#infoUnit');
    $('#infoBar').find('#infoSpearman').show();
  });
}

// zavrie vsetky okna v infoBar a bottomBar
function closeBar($localization){
  $localization.find('span').on('click', function(){
    hideDiv($localization);
    if($(this).parent().attr("id") == 'bottomBar') {
      $(this).parent().find('h1').text('');
    }
    else{
      $(this).parent().hide();
    }
  });
}

// zavrie tabulky
function closeTable($localization){
  $localization.find('span').on('click', function(){
    $(this).parent().hide();
  });
}

function setTable(){
  var table = $('#scoreTable').find('table').find('tbody')
  table.find('tr:even').find('td').addClass('scoreTable_even');
  table.find('tr:odd').find('td').addClass('scoreTable_odd');
}

function recruitUnit(){
   $('#recArcher').on('click',function(){
    $.post("main/recruit", {unit : 'Lukostrelec'});
  });
  $('#recSwordman').on('click',function(){
    $.post("main/recruit", {unit : 'Šermiar'});
  });
  $('#recSpearman').on('click',function(){
    $.post("main/recruit", {unit : 'Kopijník'});
  });
  $('#recSlinger').on('click',function(){
    $.post("main/recruit", {unit : 'Bojovník s prakom'});
  });
}

function helpTable(){
  $('#helpTable').find('ul').find('li').hover(function(){
    $(this).addClass('underline pointer');
  });
  $('#helpTable').find('ul').find('li').mouseleave(function(){
    $(this).removeClass('underline pointer');
  });
  $('#helpTable').find('ul').find('li').click(function(){
    var spec = $(this).data('resources');
    var cont =  $('#helpTable').find('#containHelpTable');
    $('#helpTable').find('#containHelpTable').find('div').remove();

    switch(spec){
      case 'resources':
        $.post('main/returnHelp',{keyword:'resources'},function(data){
          cont.append(data);
        });
        break;
      case 'buildings':
        $.post('main/returnHelp',{keyword:'buildings'},function(data){
          cont.append(data);
        });
        break;
      case 'army':
        $.post('main/returnHelp',{keyword:'army'},function(data){
          cont.append(data);
        });
        break;
      case 'build':
        $.post('main/returnHelp',{keyword:'build'},function(data){
          cont.append(data);
        });
        break;
      case 'storage':
        $.post('main/returnHelp',{keyword:'storage'},function(data){
          cont.append(data);
        });
        break;
      case 'armory':
        $.post('main/returnHelp',{keyword:'armory'},function(data){
          cont.append(data);
        });
        break;
      case 'population':
        $.post('main/returnHelp',{keyword:'population'},function(data){
          cont.append(data);
        });
        break;
      case 'quest':
        $.post('main/returnHelp',{keyword:'quest'},function(data){
          cont.append(data);
        });
        break;
    }
  });
}

$(document).ready(function(){
  hideElementAtStart();
  actionBarElementsFunction();
  infoBarBuildings();
  infoTroopsBar();
  closeBar($('#infoBar'));
  closeBar($('#bottomBar'));
  automatic_updating();
  createPlaces();
  createLandPlaces();
  setTable();
  closeTable($('#scoreTable'));
  closeTable($('#helpTable'));
  helpTable();
});