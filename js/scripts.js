﻿// JavaScript Document
$(document).ready(function(){
  $('#login').hide(); 
  $('#buildings').hide();
  $('#troops').hide();
  $('#score_table').hide();
  $('#infoBar').hide();
  $('#infoBuilding').hide();
  $('#infoUnit').hide();
  $path = $('#infoBar').find('#infoBuilding');
  $path2 = $('#infoBar').find('#infoUnit');

  
  $('#registration').find('p').addClass('pointer').on('click', function(){
    $(this).parent().hide();
    $('#login').show();
  });
  
  $('#login').find('p').addClass('pointer').on('click', function(){
    $(this).parent().hide();
    $('#registration').show();
  }); 

  $('#actionBar').find('#army').on('click', function(){
     $('#buildings').hide();
     $('#troops').show();
     $('#bottomBar').find('h1').text('Armáda');
  });

  $('#actionBar').find('#administrator').on('click', function(){
     $('#troops').hide();
  	 $('#buildings').show();
     $('#bottomBar').find('h1').text('Bodovy');
  });

  for(var i = 1; i < 11; i++) {
    $('#buildPlace'+i.toString()).on('click', function(){
      if (location.hash == '#build_place') {
        $('#buildings').show();
        $('#bottomBar').find('h1').text('Bodovy');
      }
    });
  }

  $('#bottomBar').find('span').on('click', function(){
    $(this).parent().find('h1').text('');
    $('#buildings').hide();
    $('#troops').hide();
  });

  $('#score_table').find('table').find('tbody').find('tr:even').find('td').addClass('score_table_even');
  $('#score_table').find('table').find('tbody').find('tr:odd').find('td').addClass('score_table_odd');

  $('#actionBar').find('#statistic').on('click', function(){
    $('#score_table').css({"left":"25%", "top":"25%"});
    $('#score_table').show();
    $('#score_table').draggable();
  });

  $('#score_table').find('span').on('click', function(){
    $('#score_table').hide();
  });

  $('#infoBar').find('span').on('click', function(){
    $('#infoBar').hide();
  });

  $('#bottomBar').find('#buildings').find('#townHall').on('click', function(){
    $path2.find('#infoSlinger').hide();
    $path2.find('#infoArcher').hide();
    $path2.find('#infoSwordman').hide();
    $path2.find('#infoSpearman').hide();     
    $path.find('#infoStorage').hide();
    $path.find('#infoArmory').hide();
    $path.find('#infoBurrow').hide();
    $path.find('#infoBarrack').hide();
    $path.find('#infoPlatner').hide();
    $('#infoBar').show();
    $path.show();
    $path.find('#infoTownHall').show();
  });

  $('#bottomBar').find('#buildings').find('#storage').on('click', function(){
    $path2.find('#infoSlinger').hide();
    $path2.find('#infoArcher').hide();
    $path2.find('#infoSwordman').hide();
    $path2.find('#infoSpearman').hide();
    $path.find('#infoTownHall').hide();
    $path.find('#infoArmory').hide();
    $path.find('#infoBurrow').hide();
    $path.find('#infoBarrack').hide();
    $path.find('#infoPlatner').hide();
    $('#infoBar').show();
    $path.show();
    $path.find('#infoStorage').show();
  });

  $('#bottomBar').find('#buildings').find('#armory').on('click', function(){
    $path2.find('#infoSlinger').hide();
    $path2.find('#infoArcher').hide();
    $path2.find('#infoSwordman').hide();
    $path2.find('#infoSpearman').hide();
    $path.find('#infoTownHall').hide();
    $path.find('#infoStorage').hide();
    $path.find('#infoBurrow').hide();
    $path.find('#infoBarrack').hide();
    $path.find('#infoPlatner').hide();
    $('#infoBar').show();
    $path.show();
    $path.find('#infoArmory').show();
  });

  $('#bottomBar').find('#buildings').find('#barrack').on('click', function(){
    $path2.find('#infoSlinger').hide();
    $path2.find('#infoArcher').hide();
    $path2.find('#infoSwordman').hide();
    $path2.find('#infoSpearman').hide();
    $path.find('#infoTownHall').hide();
    $path.find('#infoStorage').hide();
    $path.find('#infoArmory').hide();
    $path.find('#infoBurrow').hide();
    $path.find('#infoPlatner').hide();
    $('#infoBar').show();
    $path.show();
    $path.find('#infoBarrack').show();
  });

  $('#bottomBar').find('#buildings').find('#burrow').on('click', function(){
    $path2.find('#infoSlinger').hide();
    $path2.find('#infoArcher').hide();
    $path2.find('#infoSwordman').hide();
    $path2.find('#infoSpearman').hide();
    $path.find('#infoTownHall').hide();
    $path.find('#infoStorage').hide();
    $path.find('#infoArmory').hide();
    $path.find('#infoBarrack').hide();
    $path.find('#infoPlatner').hide();
    $('#infoBar').show();
    $path.show();
    $path.find('#infoBurrow').show();
  });

  $('#bottomBar').find('#buildings').find('#platner').on('click', function(){
    $path2.find('#infoSlinger').hide();
    $path2.find('#infoArcher').hide();
    $path2.find('#infoSwordman').hide();
    $path2.find('#infoSpearman').hide();
    $path.find('#infoTownHall').hide();
    $path.find('#infoStorage').hide();
    $path.find('#infoArmory').hide();
    $path.find('#infoBarrack').hide();
    $path.find('#infoBurrow').hide();
    $('#infoBar').show();
    $path.show();
    $path.find('#infoPlatner').show();
  });

  $('#bottomBar').find('#troops').find('#archer').on('click', function(){
    $path.find('#infoTownHall').hide();
    $path.find('#infoStorage').hide();
    $path.find('#infoArmory').hide();
    $path.find('#infoBarrack').hide();
    $path.find('#infoBurrow').hide();
    $path.find('#infoPlatner').hide();
    $path2.find('#infoSlinger').hide();
    $path2.find('#infoSpearman').hide();
    $path2.find('#infoSwordman').hide();
    $('#infoBar').show();
    $path2.show();
    $path2.find('#infoArcher').show();
  });

  $('#bottomBar').find('#troops').find('#slinger').on('click', function(){
    $path.find('#infoTownHall').hide();
    $path.find('#infoStorage').hide();
    $path.find('#infoArmory').hide();
    $path.find('#infoBarrack').hide();
    $path.find('#infoBurrow').hide();
    $path.find('#infoPlatner').hide();
    $path2.find('#infoSpearman').hide();
    $path2.find('#infoSwordman').hide();
    $path2.find('#infoArcher').hide();
    $('#infoBar').show();
    $path2.show();
    $path2.find('#infoSlinger').show();
  });

  $('#bottomBar').find('#troops').find('#spearman').on('click', function(){
    $path.find('#infoTownHall').hide();
    $path.find('#infoStorage').hide();
    $path.find('#infoArmory').hide();
    $path.find('#infoBarrack').hide();
    $path.find('#infoBurrow').hide();
    $path.find('#infoPlatner').hide();
    $path2.find('#infoSlinger').hide();
    $path2.find('#infoSwordman').hide();
    $path2.find('#infoArcher').hide();
    $('#infoBar').show();
    $path2.show();
    $path2.find('#infoSpearman').show();
  });

  $('#bottomBar').find('#troops').find('#swordman').on('click', function(){
    $path.find('#infoTownHall').hide();
    $path.find('#infoStorage').hide();
    $path.find('#infoArmory').hide();
    $path.find('#infoBarrack').hide();
    $path.find('#infoBurrow').hide();
    $path.find('#infoPlatner').hide();
    $path2.find('#infoSlinger').hide();
    $path2.find('#infoSpearman').hide();
    $path2.find('#infoArcher').hide();
    $('#infoBar').show();
    $path2.show();
    $path2.find('#infoSwordman').show();
  });

});