-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Hostiteľ: 127.0.0.1
-- Vygenerované: Po 09.Dec 2013, 19:14
-- Verzia serveru: 5.5.27
-- Verzia PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáza: `trollandgoblinswar`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `playersetting`
--

CREATE TABLE IF NOT EXISTS `playersetting` (
  `idps` int(11) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `id_nation` text NOT NULL,
  `id_genus` text NOT NULL,
  `capital` text NOT NULL,
  `pscore` int(11) NOT NULL,
  `questComplete` int(11) NOT NULL,
  PRIMARY KEY (`idps`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Sťahujem dáta pre tabuľku `playersetting`
--

INSERT INTO `playersetting` (`idps`, `username`, `id_nation`, `id_genus`, `capital`, `pscore`, `questComplete`) VALUES
(5, 'Vlado', '1', '1', 'Vladkovo', 0, 0),
(10, 'Hrac', '2', '4', 'Hracovo mesto', 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
