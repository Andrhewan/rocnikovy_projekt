-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Hostiteľ: 127.0.0.1
-- Vygenerované: Po 09.Dec 2013, 19:16
-- Verzia serveru: 5.5.27
-- Verzia PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáza: `trollandgoblinswar`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `genus_information`
--

CREATE TABLE IF NOT EXISTS `genus_information` (
  `id_gen` int(11) NOT NULL AUTO_INCREMENT,
  `id_nation` int(11) NOT NULL,
  `genus` varchar(100) COLLATE utf8_slovak_ci NOT NULL,
  `local_main_settlement` varchar(100) COLLATE utf8_slovak_ci NOT NULL,
  PRIMARY KEY (`id_gen`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci AUTO_INCREMENT=11 ;

--
-- Sťahujem dáta pre tabuľku `genus_information`
--

INSERT INTO `genus_information` (`id_gen`, `id_nation`, `genus`, `local_main_settlement`) VALUES
(1, 1, 'Hikvuhij', 'Hijniitui'),
(2, 1, 'Hahkvui', 'Jiihve'),
(3, 2, 'Khhaj', 'Tuinii'),
(4, 2, 'Ghuuniifh', 'Kvuphiurii'),
(5, 3, 'Kvikhje', 'Kvohahnii'),
(6, 3, 'Hikhve', 'Ahhghuujii'),
(7, 4, 'Ahhkhvojii', 'Riiniijii'),
(8, 4, 'Huhhjheenii', 'Huhniijii'),
(9, 5, 'Heewhuhhchrii', 'Khhohhph'),
(10, 5, 'Ahhnojii', 'Kvejhee');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
