-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Hostiteľ: 127.0.0.1
-- Vygenerované: Po 09.Dec 2013, 19:15
-- Verzia serveru: 5.5.27
-- Verzia PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáza: `trollandgoblinswar`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `nations`
--

CREATE TABLE IF NOT EXISTS `nations` (
  `id_nation` int(11) NOT NULL AUTO_INCREMENT,
  `charnat` varchar(1) COLLATE utf8_slovak_ci NOT NULL,
  `nations` varchar(100) COLLATE utf8_slovak_ci NOT NULL,
  PRIMARY KEY (`id_nation`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci AUTO_INCREMENT=6 ;

--
-- Sťahujem dáta pre tabuľku `nations`
--

INSERT INTO `nations` (`id_nation`, `charnat`, `nations`) VALUES
(1, 'g', 'Lesní goblini'),
(2, 'g', 'Severní goblini'),
(3, 'g', 'Horskí goblini'),
(4, 't', 'Goblini západných močiarov'),
(5, 't', 'Podzemní goblini');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
