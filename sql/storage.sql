-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Hostiteľ: 127.0.0.1
-- Vygenerované: Po 09.Dec 2013, 19:14
-- Verzia serveru: 5.5.27
-- Verzia PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáza: `trollandgoblinswar`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `storage`
--

CREATE TABLE IF NOT EXISTS `storage` (
  `ids` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(11) NOT NULL,
  `woods` int(11) NOT NULL,
  `stones` int(11) NOT NULL,
  `golds` int(11) NOT NULL,
  `earths` int(11) NOT NULL,
  `irons` int(11) NOT NULL,
  PRIMARY KEY (`ids`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Sťahujem dáta pre tabuľku `storage`
--

INSERT INTO `storage` (`ids`, `username`, `woods`, `stones`, `golds`, `earths`, `irons`) VALUES
(5, 'Vlado', 2000, 2000, 2000, 2000, 2000),
(10, 'Hrac', 2000, 2000, 2000, 2000, 2000);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
