-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Hostiteľ: 127.0.0.1
-- Vygenerované: Ne 19.Jan 2014, 22:43
-- Verzia serveru: 5.5.27
-- Verzia PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáza: `trollandgoblinswar`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `armory`
--

CREATE TABLE IF NOT EXISTS `armory` (
  `ida` int(11) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `bows` int(11) NOT NULL,
  `spears` int(11) NOT NULL,
  `light_armours` int(11) NOT NULL,
  `heavy_armours` int(11) NOT NULL,
  `swords` int(11) NOT NULL,
  `slings` int(11) NOT NULL,
  PRIMARY KEY (`ida`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Sťahujem dáta pre tabuľku `armory`
--

INSERT INTO `armory` (`ida`, `username`, `bows`, `spears`, `light_armours`, `heavy_armours`, `swords`, `slings`) VALUES
(5, 'Vlado', 0, 0, 0, 0, 0, 0),
(10, 'Hrac', 0, 0, 0, 0, 0, 0),
(11, 'Gab?a', 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `buildings`
--

CREATE TABLE IF NOT EXISTS `buildings` (
  `idb` int(11) NOT NULL AUTO_INCREMENT,
  `building` text COLLATE utf8_slovak_ci NOT NULL,
  `level` int(11) NOT NULL,
  `wood` int(11) NOT NULL,
  `stone` int(11) NOT NULL,
  `gold` int(11) NOT NULL,
  `iron` int(11) NOT NULL,
  `earth` int(11) NOT NULL,
  `population` int(11) NOT NULL,
  `store` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `exps` int(11) NOT NULL,
  `time` text COLLATE utf8_slovak_ci NOT NULL,
  `description` text COLLATE utf8_slovak_ci NOT NULL,
  `post` text COLLATE utf8_slovak_ci NOT NULL,
  `charnat` varchar(11) COLLATE utf8_slovak_ci NOT NULL,
  PRIMARY KEY (`idb`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci AUTO_INCREMENT=38 ;

--
-- Sťahujem dáta pre tabuľku `buildings`
--

INSERT INTO `buildings` (`idb`, `building`, `level`, `wood`, `stone`, `gold`, `iron`, `earth`, `population`, `store`, `score`, `exps`, `time`, `description`, `post`, `charnat`) VALUES
(1, 'Hlavná budova', 1, 400, 400, 500, 100, 100, 25, 0, 250, 50, '1:45', 'Hlavná budova je základom každej usadlosti. Z tohto miesta môžeš riadiť svoje mesto a mať prehľad o všetkom. ', 'buildTownHall', 'g'),
(2, 'Kasárne', 1, 250, 300, 150, 50, 75, 0, 0, 100, 25, '1:00', 'V kasárňach môžeš regrutovať vojakov. ', 'buildBarrack', 'g'),
(3, 'Nora', 1, 180, 50, 50, 10, 50, 5, 0, 10, 15, '1:00', 'V norách žijú tvoji obyvatelia. Čím vyššia úroveň nory, tým viac potrebných obyvateľov môžeš mať.', 'buildBurrow', 'g'),
(4, 'Sklad', 1, 200, 250, 150, 50, 50, 0, 3000, 50, 20, '1:15', 'V skladisku môžeš skladovať suroviny. Vyššou úrovňou skladu môžeš uskladniť viacej surovín.', 'buildStorage', 'g'),
(5, 'Dielne', 1, 150, 150, 150, 300, 50, 0, 0, 50, 25, '3:00', 'V dielni môžeš vyrábať brnenia pre svoje vojská. Čím vyššia úroveň dielne, tým odolnejšie brnenie môžeš mať.', 'buildArmoryWork', 'g'),
(6, 'Hlavná budova', 2, 1000, 1000, 1200, 750, 0, 100, 0, 300, 0, '', 'Hlavná budova je základom každej usadlosti. Z tohto miesta môžeš riadiť svoje mesto a mať prehľad o všetkom. ', '', 'g'),
(7, 'Hlavná budova', 3, 2000, 2000, 2300, 2000, 0, 250, 0, 750, 0, '', 'Hlavná budova je základom každej usadlosti. Z tohto miesta môžeš riadiť svoje mesto a mať prehľad o všetkom. ', '', 'g'),
(8, 'Hlavná budova', 4, 3000, 3000, 3250, 2100, 0, 500, 0, 300, 0, '', 'Hlavná budova je základom každej usadlosti. Z tohto miesta môžeš riadiť svoje mesto a mať prehľad o všetkom. ', '', 'g'),
(9, 'Kasárne', 2, 350, 400, 200, 100, 0, 0, 0, 150, 0, '', 'V kasárňach môžeš regrutovať vojakov. ', '', 'g'),
(10, 'Kasárne', 3, 450, 600, 400, 200, 0, 0, 0, 175, 0, '', 'V kasárňach môžeš regrutovať vojakov. ', '', 'g'),
(11, 'Kasárne', 4, 600, 750, 300, 250, 0, 0, 0, 200, 0, '', 'V kasárňach môžeš regrutovať vojakov. ', '', 'g'),
(12, 'Dielňa na výrobu brnenia', 2, 200, 200, 200, 400, 0, 0, 0, 75, 0, '', 'V dielni môžeš vyrábať brnenia pre svoje vojská. Čím vyššia úroveň dielne, tým odolnejšie brnenie môžeš mať.', '', 'g'),
(13, 'Dielňa na výrobu brnenia', 3, 300, 300, 300, 600, 0, 0, 0, 100, 0, '', 'V dielni môžeš vyrábať brnenia pre svoje vojská. Čím vyššia úroveň dielne, tým odolnejšie brnenie môžeš mať.', '', 'g'),
(14, 'Sklad', 2, 250, 300, 150, 100, 0, 0, 6000, 75, 0, '', 'V skladisku môžeš skladovať suroviny. Vyššou úrovňou skladu môžeš uskladniť viacej surovín.', '', 'g'),
(15, 'Sklad', 3, 300, 350, 200, 150, 0, 0, 11000, 100, 0, '', 'V skladisku môžeš skladovať suroviny. Vyššou úrovňou skladu môžeš uskladniť viacej surovín.', '', 'g'),
(16, 'Sklad', 4, 400, 500, 350, 200, 0, 0, 20000, 125, 0, '', 'V skladisku môžeš skladovať suroviny. Vyššou úrovňou skladu môžeš uskladniť viacej surovín.', '', 'g'),
(17, 'Sklad', 5, 500, 650, 400, 300, 0, 0, 30000, 150, 0, '', 'V skladisku môžeš skladovať suroviny. Vyššou úrovňou skladu môžeš uskladniť viacej surovín.', '', 'g'),
(18, 'Sklad', 6, 650, 800, 600, 400, 0, 0, 40000, 200, 0, '', 'V skladisku môžeš skladovať suroviny. Vyššou úrovňou skladu môžeš uskladniť viacej surovín.', '', 'g'),
(19, 'Nora', 2, 200, 75, 75, 25, 0, 15, 0, 20, 0, '', 'V norách žijú tvoji obyvatelia. Čím vyššia úroveň nory, tým viac potrebných obyvateľov môžeš mať.', '', 'g'),
(20, 'Nora', 3, 225, 100, 100, 50, 0, 25, 0, 25, 0, '', 'V norách žijú tvoji obyvatelia. Čím vyššia úroveň nory, tým viac potrebných obyvateľov môžeš mať.', '', 'g'),
(21, 'Nora', 4, 250, 150, 150, 75, 0, 35, 0, 30, 0, '', 'V norách žijú tvoji obyvatelia. Čím vyššia úroveň nory, tým viac potrebných obyvateľov môžeš mať.', '', 'g'),
(22, 'Nora', 5, 275, 175, 175, 100, 0, 45, 0, 35, 0, '', 'V norách žijú tvoji obyvatelia. Čím vyššia úroveň nory, tým viac potrebných obyvateľov môžeš mať.', '', 'g'),
(23, 'Nora', 6, 300, 200, 200, 125, 0, 55, 0, 40, 0, '', 'V norách žijú tvoji obyvatelia. Čím vyššia úroveň nory, tým viac potrebných obyvateľov môžeš mať.', '', 'g'),
(24, 'Nora', 7, 350, 300, 250, 150, 0, 65, 0, 50, 0, '', 'V norách žijú tvoji obyvatelia. Čím vyššia úroveň nory, tým viac potrebných obyvateľov môžeš mať.', '', 'g'),
(25, 'Nora', 8, 425, 350, 300, 175, 0, 75, 0, 60, 0, '', 'V norách žijú tvoji obyvatelia. Čím vyššia úroveň nory, tým viac potrebných obyvateľov môžeš mať.', '', 'g'),
(26, 'Nora', 9, 550, 475, 400, 200, 0, 85, 0, 70, 0, '', 'V norách žijú tvoji obyvatelia. Čím vyššia úroveň nory, tým viac potrebných obyvateľov môžeš mať.', '', 'g'),
(27, 'Nora', 10, 700, 600, 550, 225, 0, 100, 0, 80, 0, '', 'V norách žijú tvoji obyvatelia. Čím vyššia úroveň nory, tým viac potrebných obyvateľov môžeš mať.', '', 'g'),
(28, 'Zbrojnica', 1, 250, 250, 150, 250, 0, 0, 0, 25, 15, '', 'V zbrojnici skladuješ zbrane pre tvoje vojsko.', 'buildArmory', 'g'),
(29, 'Zbrojnica', 2, 300, 300, 200, 300, 0, 0, 0, 35, 0, '', 'V zbrojnici skladuješ zbrane pre tvoje vojsko.', '', 'g'),
(30, 'Zbrojnica', 3, 350, 350, 250, 350, 0, 0, 0, 45, 0, '', 'V zbrojnici skladuješ zbrane pre tvoje vojsko.', '', 'g'),
(31, 'Dielňa na výrobu strelných zbraní', 1, 250, 100, 150, 50, 0, 0, 0, 50, 0, '', 'V dielni môžeš vyrábať strelné zbrane pre svoje vojská.', '', '0'),
(32, 'Dielňa na výrobu strelných zbraní', 2, 300, 200, 250, 100, 0, 0, 0, 60, 0, '', 'V dielni môžeš vyrábať strelné zbrane pre svoje vojská.', '', '0'),
(33, 'Kruhový oltár', 1, 100, 500, 250, 500, 0, 0, 0, 50, 0, '', 'Kruhový oltár zvyšuje náboženskú silu a vplyv v tvojom meste. Je dôležité postaviť kruhový oltár v usadlosti, aby si mohol stavať ďalšie miesta k rozvoju kultúry a náboženstva.', '', '0'),
(34, 'Strom vedomia', 1, 500, 150, 100, 0, 0, 0, 0, 50, 0, '', 'Strom vedomia je miesto, kde prebiehajú obrady, stretávajú sa šamani a mudrci z celého okolia tvojej usadlosti. Je to jedinečná budova iba pre goblinov.', '', '0'),
(35, 'Strom vedomia', 2, 600, 200, 150, 0, 0, 0, 0, 100, 0, '', 'Strom vedomia je miesto, kde prebiehajú obrady, stretávajú sa šamani a mudrci z celého okolia tvojej usadlosti. Je to jedinečná budova iba pre goblinov.', '', '0'),
(36, 'Strom vedomia', 3, 700, 250, 250, 0, 0, 0, 0, 150, 0, '', 'Strom vedomia je miesto, kde prebiehajú obrady, stretávajú sa šamani a mudrci z celého okolia tvojej usadlosti. Je to jedinečná budova iba pre goblinov.', '', '0'),
(37, 'Námestie', 1, 300, 300, 300, 300, 0, 50, 0, 35, 0, '', 'Námestie je miesto, kde sa stretávajú všetci obyvatelia tvojej usadlosti. Taktiež poskytuje miesto pre nových obyvateľov.', '', '0');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `buildplace`
--

CREATE TABLE IF NOT EXISTS `buildplace` (
  `idbp` int(11) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `placeOne` text NOT NULL,
  `placeTwo` text NOT NULL,
  `placeThree` text NOT NULL,
  `placeFour` text NOT NULL,
  `placeFive` text NOT NULL,
  `placeSix` text NOT NULL,
  `placeSeven` text NOT NULL,
  `placeEight` text NOT NULL,
  `placeNine` text NOT NULL,
  `placeTen` text NOT NULL,
  `placeEleven` text NOT NULL,
  `placeTwelve` text NOT NULL,
  `placeThirteen` text NOT NULL,
  `placeFourtheen` text NOT NULL,
  `placeFifthteen` text NOT NULL,
  `placeEighteen` text NOT NULL,
  `placeNinetheen` text NOT NULL,
  `placeTwenty` text NOT NULL,
  PRIMARY KEY (`idbp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Sťahujem dáta pre tabuľku `buildplace`
--

INSERT INTO `buildplace` (`idbp`, `username`, `placeOne`, `placeTwo`, `placeThree`, `placeFour`, `placeFive`, `placeSix`, `placeSeven`, `placeEight`, `placeNine`, `placeTen`, `placeEleven`, `placeTwelve`, `placeThirteen`, `placeFourtheen`, `placeFifthteen`, `placeEighteen`, `placeNinetheen`, `placeTwenty`) VALUES
(5, 'Vlado', 'Nora', 'Nora', 'Kasárne', 'Dielne', 'Zbrojnica', 'Hlavná budova', 'Dielne', 'Sklad', 'Kasárne', 'Sklad', '', '', '', '', '', '', '', ''),
(10, 'Hrac', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11, 'Gab?a', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `experience`
--

CREATE TABLE IF NOT EXISTS `experience` (
  `idE` int(11) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `exp` int(11) NOT NULL,
  PRIMARY KEY (`idE`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Sťahujem dáta pre tabuľku `experience`
--

INSERT INTO `experience` (`idE`, `username`, `exp`) VALUES
(5, 'Vlado', 0),
(10, 'Hrac', 0),
(11, 'Gab?a', 0);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `genus_information`
--

CREATE TABLE IF NOT EXISTS `genus_information` (
  `id_gen` int(11) NOT NULL AUTO_INCREMENT,
  `id_nation` int(11) NOT NULL,
  `genus` varchar(100) COLLATE utf8_slovak_ci NOT NULL,
  `local_main_settlement` varchar(100) COLLATE utf8_slovak_ci NOT NULL,
  PRIMARY KEY (`id_gen`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci AUTO_INCREMENT=11 ;

--
-- Sťahujem dáta pre tabuľku `genus_information`
--

INSERT INTO `genus_information` (`id_gen`, `id_nation`, `genus`, `local_main_settlement`) VALUES
(1, 1, 'Hikvuhij', 'Hijniitui'),
(2, 1, 'Hahkvui', 'Jiihve'),
(3, 2, 'Khhaj', 'Tuinii'),
(4, 2, 'Ghuuniifh', 'Kvuphiurii'),
(5, 3, 'Kvikhje', 'Kvohahnii'),
(6, 3, 'Hikhve', 'Ahhghuujii'),
(7, 4, 'Ahhkhvojii', 'Riiniijii'),
(8, 4, 'Huhhjheenii', 'Huhniijii'),
(9, 5, 'Heewhuhhchrii', 'Khhohhph'),
(10, 5, 'Ahhnojii', 'Kvejhee');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `nations`
--

CREATE TABLE IF NOT EXISTS `nations` (
  `id_nation` int(11) NOT NULL AUTO_INCREMENT,
  `charnat` varchar(1) COLLATE utf8_slovak_ci NOT NULL,
  `nations` varchar(100) COLLATE utf8_slovak_ci NOT NULL,
  PRIMARY KEY (`id_nation`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci AUTO_INCREMENT=6 ;

--
-- Sťahujem dáta pre tabuľku `nations`
--

INSERT INTO `nations` (`id_nation`, `charnat`, `nations`) VALUES
(1, 'g', 'Lesní goblini'),
(2, 'g', 'Severní goblini'),
(3, 'g', 'Horskí goblini'),
(4, 't', 'Goblini západných močiarov'),
(5, 't', 'Podzemní goblini');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `playersetting`
--

CREATE TABLE IF NOT EXISTS `playersetting` (
  `idps` int(11) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `id_nation` text NOT NULL,
  `id_genus` text NOT NULL,
  `capital` text NOT NULL,
  `pscore` int(11) NOT NULL,
  `questComplete` int(11) NOT NULL,
  PRIMARY KEY (`idps`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Sťahujem dáta pre tabuľku `playersetting`
--

INSERT INTO `playersetting` (`idps`, `username`, `id_nation`, `id_genus`, `capital`, `pscore`, `questComplete`) VALUES
(5, 'Vlado', '1', '1', 'Vladkovo', 0, 0),
(10, 'Hrac', '2', '4', 'Hracovo mesto', 0, 0),
(11, 'Gab?a', '3', '6', 'Wufre', 0, 0);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `quests`
--

CREATE TABLE IF NOT EXISTS `quests` (
  `no` int(11) NOT NULL,
  `header` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `text` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `needed` text NOT NULL,
  `rewardexp` int(11) NOT NULL,
  `rewwood` int(11) NOT NULL,
  `rewstone` int(11) NOT NULL,
  `rewearth` int(11) NOT NULL,
  `rewgold` int(11) NOT NULL,
  `rewiron` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `quests`
--

INSERT INTO `quests` (`no`, `header`, `text`, `needed`, `rewardexp`, `rewwood`, `rewstone`, `rewearth`, `rewgold`, `rewiron`) VALUES
(1, 'Nový začiatok', 'Je čas začať budovať svoju usadlosť. Postav Hlavnú budovu a získaj odmenu.', 'Postav Hlavnú budovu', 50, 0, 0, 0, 0, 0),
(2, 'Rozšírenie zásob', 'Aby si mohol skladovať viacej surovín, musíš postaviť sklad.', 'Postav Sklad', 15, 200, 200, 200, 200, 200);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `storage`
--

CREATE TABLE IF NOT EXISTS `storage` (
  `ids` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(11) NOT NULL,
  `woods` int(11) NOT NULL,
  `stones` int(11) NOT NULL,
  `golds` int(11) NOT NULL,
  `earths` int(11) NOT NULL,
  `irons` int(11) NOT NULL,
  PRIMARY KEY (`ids`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Sťahujem dáta pre tabuľku `storage`
--

INSERT INTO `storage` (`ids`, `username`, `woods`, `stones`, `golds`, `earths`, `irons`) VALUES
(5, 'Vlado', 2000, 2000, 2000, 2000, 2000),
(10, 'Hrac', 2000, 2000, 2000, 2000, 2000),
(11, 'Gab?a', 2000, 2000, 2000, 2000, 2000);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tips`
--

CREATE TABLE IF NOT EXISTS `tips` (
  `no` int(11) NOT NULL,
  `describe` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `tips`
--

INSERT INTO `tips` (`no`, `describe`) VALUES
(1, 'Každý druh má vlastné bonusy. Objav ich posilni vplyv svojho druhu v okolí.'),
(2, 'Zvyšuj úrovne ťažobných budov a získaj viacej surovín.');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `troops`
--

CREATE TABLE IF NOT EXISTS `troops` (
  `idt` int(11) NOT NULL,
  `unit` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `bow` int(11) NOT NULL,
  `sling` int(11) NOT NULL,
  `spear` int(11) NOT NULL,
  `sword` int(11) NOT NULL,
  `light_armour` int(11) NOT NULL,
  `heavy_armour` int(11) NOT NULL,
  `gold` int(11) NOT NULL,
  `time` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `post` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `charnat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `troops`
--

INSERT INTO `troops` (`idt`, `unit`, `bow`, `sling`, `spear`, `sword`, `light_armour`, `heavy_armour`, `gold`, `time`, `description`, `post`, `charnat`) VALUES
(1, 'Lukostrelec', 1, 0, 0, 0, 1, 0, 100, '0:25', 'Lukostrelci sú jednotky, ktoré dokáže z veľkej vzdialenosti účinne ničiť nepriateľské ciele. ', 'recArcher', 'g'),
(2, 'Bojovník s prakom', 0, 1, 0, 0, 1, 0, 30, '0:20', 'Bojovník s prakom je ľahká jednotka, ktorá je vhodná na ľahko obrnené ciele.', 'recSlinger', 'g'),
(3, 'Kopijník', 0, 0, 1, 0, 0, 1, 300, '0:50', 'Kopijníci sú ťažká jednotka vhodná na udržiavanie formácie na bojovej línii. Dlhé kópie prepichnú nepriateľa skôr ako sa k nim priblíži.', 'recSpearman', 'g'),
(4, 'Tažkoodenec', 0, 0, 0, 1, 0, 1, 390, '1:05', 'Ťažká bojová jednotka, ktorá je základom každej armády. Húževnatí bojovníci sú skoro nezraniteľní v ich ťažkom brnení a mečmi dokážu rozseknúť nejednu hlavu.', 'recSwordman', 'g');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `userinformation`
--

CREATE TABLE IF NOT EXISTS `userinformation` (
  `idui` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `userpassword` varchar(50) CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  PRIMARY KEY (`idui`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Sťahujem dáta pre tabuľku `userinformation`
--

INSERT INTO `userinformation` (`idui`, `username`, `userpassword`) VALUES
(3, 'Vlado', '47a21a434edabe1abea6e6ec4d2f20cb'),
(8, 'Hrac', '003f99a2272ad3a6afc33adbe195a801'),
(9, 'Gab?a', '5e2b9f5f977c67bf309fbdf68cb394b9');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `yourunits`
--

CREATE TABLE IF NOT EXISTS `yourunits` (
  `idu` int(11) NOT NULL AUTO_INCREMENT,
  `username` text CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `slingers` int(11) NOT NULL,
  `archers` int(11) NOT NULL,
  `swordmen` int(11) NOT NULL,
  `spearmen` int(11) NOT NULL,
  PRIMARY KEY (`idu`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Sťahujem dáta pre tabuľku `yourunits`
--

INSERT INTO `yourunits` (`idu`, `username`, `slingers`, `archers`, `swordmen`, `spearmen`) VALUES
(1, 'Vlado', 0, 0, 0, 0),
(2, 'Hrac', 0, 0, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
