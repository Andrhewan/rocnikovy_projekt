-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Hostiteľ: 127.0.0.1
-- Vygenerované: Po 09.Dec 2013, 19:16
-- Verzia serveru: 5.5.27
-- Verzia PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáza: `trollandgoblinswar`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `buildplace`
--

CREATE TABLE IF NOT EXISTS `buildplace` (
  `idbp` int(11) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `placeOne` int(11) NOT NULL,
  `placeTwo` int(11) NOT NULL,
  `placeThree` int(11) NOT NULL,
  `placeFour` int(11) NOT NULL,
  `placeFive` int(11) NOT NULL,
  `placeSix` int(11) NOT NULL,
  `placeSeven` int(11) NOT NULL,
  `placeEight` int(11) NOT NULL,
  `placeNine` int(11) NOT NULL,
  `placeTen` int(11) NOT NULL,
  `placeEleven` int(11) NOT NULL,
  `placeTwelve` int(11) NOT NULL,
  `placeThirteen` int(11) NOT NULL,
  `placeFourtheen` int(11) NOT NULL,
  `placeFifthteen` int(11) NOT NULL,
  `placeEighteen` int(11) NOT NULL,
  `placeNinetheen` int(11) NOT NULL,
  `placeTwenty` int(11) NOT NULL,
  PRIMARY KEY (`idbp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Sťahujem dáta pre tabuľku `buildplace`
--

INSERT INTO `buildplace` (`idbp`, `username`, `placeOne`, `placeTwo`, `placeThree`, `placeFour`, `placeFive`, `placeSix`, `placeSeven`, `placeEight`, `placeNine`, `placeTen`, `placeEleven`, `placeTwelve`, `placeThirteen`, `placeFourtheen`, `placeFifthteen`, `placeEighteen`, `placeNinetheen`, `placeTwenty`) VALUES
(5, 'Vlado', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(10, 'Hrac', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
