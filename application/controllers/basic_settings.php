<?php if(!defined("BASEPATH")) exit("No direct script acces allowed");

class Basic_settings extends CI_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged')) {
			redirect('login');
		}
		$this->load->model('settings_model');
	}

	function settings(){
		$data['goblins'] = $this->settings_model->select_nation('g');
		$data['trolls'] = $this->settings_model->select_nation('t');
		if ($this->settings_model->checkSetting('id_nation') == 0) {
			$this->form_validation->set_rules('nation','Národ','required');
			$this->form_validation->set_error_delimiters('<p class="error">','</p>');
			if($this->form_validation->run()){
				$this->settings_model->saveSetting('id_nation',$_POST['nation']);
				redirect('settings');
			}
			$this->template->set('title','Nastavenia');
			$this->template->view('basic_settings_view',$data);
		}
//		print_r( $this->session->all_userdata()['id']);
		else if ($this->settings_model->checkSetting('id_genus') == 0 or $this->settings_model->checkSetting('capital') == '') {
				$data2['genus'] = $this->settings_model->select_genus($this->settings_model->checkSetting('id_nation'));
				$this->form_validation->set_rules('town','Mesto','trim|required');
				$this->form_validation->set_error_delimiters('<p class="error">','</p>');
				if($this->form_validation->run()){
					$this->settings_model->saveSetting('id_genus',$_POST['genus']);
					$this->settings_model->saveSetting('capital',$_POST['town']);
					redirect('townmode');
				}
			$this->template->set('title','Nastavenia');
			$this->template->view('basic_settings2_view',$data2);
			}
		else {
			redirect('townmode');
		}
	}
}