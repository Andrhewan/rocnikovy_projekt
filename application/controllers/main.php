<?php if(!defined("BASEPATH")) exit("No direct script acces allowed");

class Main extends CI_Controller {
	function __construct(){
		parent::__construct();
		if(!$this->session->userdata('logged')) {
			redirect('login');
		}
		$this->load->model('main_model');
		$this->load->helper('language');
		$this->lang->load('game');
	}
	function townmode(){
		$column = $this->main_model->selectPlayerData()[0]['id_nation'];
		$data['data'] = $this->main_model->selectInfo('nations','id_nation',$column,'nations');
		$column = $this->main_model->selectPlayerData()[0]['id_genus'];
		$data['data2'] = $this->main_model->selectInfo('genus_information','id_gen',$column,'genus');
		$data['data3'] = $this->main_model->selectPlayerData()[0]['capital'];
		$data['res'] = $this->main_model->returnResource2();
		$data['exp'] = $this->main_model->giveExpPlayer();
		$data['player'] = $this->session->all_userdata();
		$data['players'] = $this->main_model->returnPlayer();
		$data['buildings'] = $this->main_model->showBuildings();
		$data['troops'] = $this->main_model->showTroops();
		$data['bs'] = $this->main_model->returnBasicGameSettings();
		$this->template->set('title','Vojna Trollov a Goblinov');
		$this->template->view('main_view',$data);
	}

	function worldmode(){
		$column = $this->main_model->selectPlayerData()[0]['id_nation'];
		$data['data'] = $this->main_model->selectInfo('nations','id_nation',$column,'nations');
		$column = $this->main_model->selectPlayerData()[0]['id_genus'];
		$data['data2'] = $this->main_model->selectInfo('genus_information','id_gen',$column,'genus');
		$data['data3'] = $this->main_model->selectPlayerData()[0]['capital'];
		$data['res'] = $this->main_model->returnResource2();
		$data['exp'] = $this->main_model->giveExpPlayer();
		$data['player'] = $this->session->all_userdata();
		$data['players'] = $this->main_model->returnPlayer();
		$data['buildings'] = $this->main_model->showBuildings();
		$data['troops'] = $this->main_model->showTroops();
		$this->template->set('title','Mapa sveta');
		$this->template->view('world_view',$data);
	}

	function landmode(){
		$column = $this->main_model->selectPlayerData()[0]['id_nation'];
		$data['data'] = $this->main_model->selectInfo('nations','id_nation',$column,'nations');
		$column = $this->main_model->selectPlayerData()[0]['id_genus'];
		$data['data2'] = $this->main_model->selectInfo('genus_information','id_gen',$column,'genus');
		$data['data3'] = $this->main_model->selectPlayerData()[0]['capital'];
		$data['res'] = $this->main_model->returnResource2();
		$data['exp'] = $this->main_model->giveExpPlayer();
		$data['player'] = $this->session->all_userdata();
		$data['players'] = $this->main_model->returnPlayer();
		$data['buildings'] = $this->main_model->showBuildings();
		$data['troops'] = $this->main_model->showTroops();
		$data['bs'] = $this->main_model->returnBasicGameSettings();
		$this->template->set('title','Mapa okolia');
		$this->template->view('land_view',$data);
	}

	function battle_mode(){
		$this->template->set('title','Bojová kalkulačka');
		$this->template->view('battle_view');
	}

	function update(){
		$this->main_model->update_all();
	}

	function returnAllId(){
		$this->main_model->return_all_id();
	}

	function returnWood(){
		$this->main_model->returnResource('wood');
	}

	function returnStone(){
		$this->main_model->returnResource('stone');
	}

	function returnGold(){
		$this->main_model->returnResource('gold');
	}

	function returnIron(){
		$this->main_model->returnResource('iron');
	}

	function returnEarth(){
		$this->main_model->returnResource('earth');
	}

	function buildPlaces(){
		$this->main_model->createBuildPlace();
	}

	function buildLandPlaces(){
		$this->main_model->createBuildLandPlace();
	}

	function build(){
		$this->main_model->verify($_POST['building'],$_POST['placeid']);
	}

	function recruit($unit){
		$this->form_validation->set_rules('recArcher_val', 'Lukostrelec', 'trim|integer');
		$this->form_validation->set_rules('recSlinger_val', 'Bojovník s prakom', 'trim|integer');
		$this->form_validation->set_rules('recSpearman_val', 'Kopijník', 'trim|integer');
		$this->form_validation->set_rules('recSwordman_val', 'Ťažkoodenec', 'trim|integer');
		$this->form_validation->set_error_delimiters('<p class="error">','</p>');
		if($this->form_validation->run()) {
			$this->main_model->verifyUnit($unit,$_POST[$unit.'_val']);
			redirect('townmode');
		}
	}

	function help(){
		$this->load->view('help_view');
	}

	function verifyBuilding(){
		$this->main_model->verify($_POST['building'],$_POST['placeid']);
	}

	function returnHelp(){
		$this->main_model->returnHelp($_POST['keyword']);
	}

	function updateStorageCapacity() {
		$this->main_model->incStorageCapacity($_POST['capacity']);
	}

	function returnUpdate(){
		$this->main_model->returnUpdate($_POST['select']);
	}

	function evaluating($data){
		foreach($data['troop_val'] as $tv):
			switch ($tv['unit']){
				case 'Lukostrelec':
					$data['arch'] = $data['arch']*($tv['defense']+$tv['attack']);
					$data['arch1'] = $data['arch1']*($tv['defense']+$tv['attack']);
					break;
				case 'Kopijník':
					$data['spea'] =  $data['spea']*($tv['defense']+$tv['attack']);
					$data['spea1'] =  $data['spea1']*($tv['defense']+$tv['attack']);
					break;
				case 'Bojovník s prakom':
					$data['slin'] = $data['slin']*($tv['defense']+$tv['attack']);
					$data['slin1'] = $data['slin1']*($tv['defense']+$tv['attack']);
					break;
				case 'Ťažkoodenec':
					$data['swor'] = $data['swor']*($tv['defense']+$tv['attack']);
					$data['swor1'] = $data['swor1']*($tv['defense']+$tv['attack']);
					break;
			}
		endforeach;
		$army1 = $data['arch']+$data['slin']+$data['spea']+$data['swor'];
		$army2 = $data['arch1']+$data['slin1']+$data['spea1']+$data['swor1'];
		print($army1);
		print($army2);
		$data = array(
			'power' => $army1,
			'power2' => $army2,
			);
		if ($army1 > $army2) {
			$data['bool'] = True;
			return $data;
		}
		$data['bool'] = False;
		return $data;
	}

	function eval_battle(){
		$this->form_validation->set_rules('archer_val', 'Lukostrelec (Tvoj)', 'trim|integer');
		$this->form_validation->set_rules('archer_val1', 'Lukostrelec (Nepriateľ)', 'trim|integer');
		$this->form_validation->set_rules('sling_val', 'Bojovník s prakom (Tvoj)', 'trim|integer');
		$this->form_validation->set_rules('sling_val1', 'Bojovník s prakom (Nepriateľ)', 'trim|integer');
		$this->form_validation->set_rules('spear_val', 'Kopijník (Tvoj)', 'trim|integer');
		$this->form_validation->set_rules('spear_val1', 'Kopijník (Nepriateľ)', 'trim|integer');
		$this->form_validation->set_rules('sword_val', 'Ťažkoodenec (Tvoj)', 'trim|integer');
		$this->form_validation->set_rules('sword_val1', 'Ťažkoodenec (Nepriateľ)', 'trim|integer');
		$this->form_validation->set_error_delimiters('<p class="error">','</p>');
		if($this->form_validation->run()) {
			$data = array(
				'arch' => $_POST['archer_val'],
				'slin' => $_POST['sling_val'],
				'spea' => $_POST['spear_val'],
				'swor' => $_POST['sword_val'],
				'arch1' => $_POST['archer_val1'],
				'slin1' => $_POST['sling_val1'],
				'spea1' => $_POST['spear_val1'],
				'swor1' => $_POST['sword_val1'],
				'troop_val' => $this->main_model->showTroops(),
				);
			if($this->evaluating($data)['bool']){
				$this->session->set_flashdata('win_message','<p class="ok">Vyhral si!</p><p>Sila tvojho vojska:'.$this->evaluating($data)['power'].'</p><p>Sila nepriateľského vojska:'.$this->evaluating($data)['power2'].'</p>');
			}
			else{
				$this->session->set_flashdata('loose_message','<p class="loose">Prehral si!</p><p>Sila tvojho vojska:'.$this->evaluating($data)['power'].'</p><p>Sila nepriateľského vojska:'.$this->evaluating($data)['power2'].'</p>');
			}
			redirect('battle');
		}
		$this->battle_mode();
	}

	function soldiers(){
		$this->main_model->return_count_soldiers();
	}

	function troops(){
		$this->main_model->returnTroops();
	}

	function return_weapons(){
		$this->main_model->return_weapons();
	}
}
?>