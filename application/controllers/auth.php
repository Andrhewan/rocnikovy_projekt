<?php if(!defined("BASEPATH")) exit("No direct script acces allowed");

class Auth extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('auth_model','');
		$this->load->helper('language');
		$this->lang->load('game');
	}

	function index() {
		$this->template->set('title',lang('register'));
		$this->template->view('register_view');
	}

	function register() {
		$this->load->model('main_model');
		$this->form_validation->set_rules('registerName', lang('pName'), 'trim|required|strip_tags');
		$this->form_validation->set_rules('registerPassword', lang('pPass'), 'trim|required|min_length[5]|strip_tags');
		$this->form_validation->set_error_delimiters('<p class="error">','</p>');
		if($this->form_validation->run()) {
			if(!$this->auth_model->checkNewUser() && $this->auth_model->register()){
				$id['id'] = $this->db->insert_id();
				$this->auth_model->setuID('player_data',$id['id']);
				$this->auth_model->setuID('resources_data',$id['id']);
				$this->auth_model->setuID('experience',$id['id']);
				$this->auth_model->setuID('buildplace',$id['id']);
				$this->auth_model->setuID('buildplacelevel',$id['id']);
				$this->auth_model->setuID('landbuildplace',$id['id']);
				$this->auth_model->setuID('armory',$id['id']);
				$this->auth_model->setuID('player_units',$id['id']);
				$this->auth_model->setuID('base_game_setting',$id['id']);
				$this->main_model->basicGameSettings($id['id']);
				$this->main_model->giveResource(2000,$id['id']);
				redirect('login');
			}
			else {
				$this->session->set_flashdata('exist_error_message','<p class="error">'.sprintf(lang('isRegistered'),$_POST['registerName']).'</p>');
				redirect('register');
			}
		}
		$this->template->set('title', lang('register'));
		$this->template->view('register_view');
	}

	function login() {
		$this->form_validation->set_rules('loginName', lang('pName'), 'trim|required');
		$this->form_validation->set_rules('loginPassword', lang('pPass'), 'trim|required');
		$this->form_validation->set_error_delimiters('<p class="error">','</p>');
		if($this->form_validation->run()){
			print_r($this->auth_model->check());
			if ($this->auth_model->check()){
				$data = $this->auth_model->getUserData($_POST['loginName']);
				$data['logged'] = TRUE;

				$this->session->set_userdata($data);
				redirect('settings');
				//print_r( $this->session->all_userdata()['id']);
			}
			else {
				$this->session->set_flashdata('register_error_message','<p class="error">'.lang('notRegister').'</p>');
				redirect('login');
			}
		}
		$this->template->set('title', lang('login'));
		$this->template->view('login_view');
	}

	function logout(){
		$this->session->unset_userdata(array('id'=>'','username'=>'','userpassword'=>'','logged'=>''));
		$this->login();
	}

	function user_settings(){
		$this->form_validation->set_rules('newPassword', 'Heslo 1', 'trim|required|min_length[5],');
		$this->form_validation->set_rules('newPassword2', 'Heslo 2', 'trim|required|matches[newPassword]');
		$this->form_validation->set_error_delimiters('<p class="error">','</p>');
		if($this->form_validation->run()) {
			$this->auth_model->update_password();
			$this->session->set_flashdata('succ_message','<p class="ok">'.lang("okChangePass").'</p>');
			redirect('setuser');
		}
		else {
			$this->session->set_flashdata('error_message','<p class="error">'.lang("errorOccure").'</p>');
		}
		$this->template->set('title',lang('changePassword'));
		$this->template->view('user_settings_view');
	}

	function rsr(){
		$this->template->set('title','Špecifikácia');
		$this->template->view('rsr_view');
	}
}

?>



	function __construct(){
		parent::__construct();
		if(!$this->session->userdata('logged')) {
			redirect('login');
		}
		$this->load->model('main_model');
		$this->load->helper('language');
		$this->lang->load('game');
	}
	

	

	function recruit($unit){
		$this->form_validation->set_rules('recArcher_val', 'Lukostrelec', 'trim|integer');
		$this->form_validation->set_rules('recSlinger_val', 'Bojovník s prakom', 'trim|integer');
		$this->form_validation->set_rules('recSpearman_val', 'Kopijník', 'trim|integer');
		$this->form_validation->set_rules('recSwordman_val', 'Ťažkoodenec', 'trim|integer');
		$this->form_validation->set_error_delimiters('<p class="error">','</p>');
		if($this->form_validation->run()) {
			$this->main_model->verifyUnit($unit,$_POST[$unit.'_val']);
			redirect('townmode');
		}
	}

	

	

	

	function updateStorageCapacity() {
		$this->main_model->incStorageCapacity($_POST['capacity']);
	}

	function returnUpdate(){
		$this->main_model->returnUpdate($_POST['select']);
	}

	

	function eval_battle(){
		$this->form_validation->set_rules('archer_val', 'Lukostrelec (Tvoj)', 'trim|integer');
		$this->form_validation->set_rules('archer_val1', 'Lukostrelec (Nepriateľ)', 'trim|integer');
		$this->form_validation->set_rules('sling_val', 'Bojovník s prakom (Tvoj)', 'trim|integer');
		$this->form_validation->set_rules('sling_val1', 'Bojovník s prakom (Nepriateľ)', 'trim|integer');
		$this->form_validation->set_rules('spear_val', 'Kopijník (Tvoj)', 'trim|integer');
		$this->form_validation->set_rules('spear_val1', 'Kopijník (Nepriateľ)', 'trim|integer');
		$this->form_validation->set_rules('sword_val', 'Ťažkoodenec (Tvoj)', 'trim|integer');
		$this->form_validation->set_rules('sword_val1', 'Ťažkoodenec (Nepriateľ)', 'trim|integer');
		$this->form_validation->set_error_delimiters('<p class="error">','</p>');
		if($this->form_validation->run()) {
			$data = array(
				'arch' => $_POST['archer_val'],
				'slin' => $_POST['sling_val'],
				'spea' => $_POST['spear_val'],
				'swor' => $_POST['sword_val'],
				'arch1' => $_POST['archer_val1'],
				'slin1' => $_POST['sling_val1'],
				'spea1' => $_POST['spear_val1'],
				'swor1' => $_POST['sword_val1'],
				'troop_val' => $this->main_model->showTroops(),
				);
			if($this->evaluating($data)['bool']){
				$this->session->set_flashdata('win_message','<p class="ok">Vyhral si!</p><p>Sila tvojho vojska:'.$this->evaluating($data)['power'].'</p><p>Sila nepriateľského vojska:'.$this->evaluating($data)['power2'].'</p>');
			}
			else{
				$this->session->set_flashdata('loose_message','<p class="loose">Prehral si!</p><p>Sila tvojho vojska:'.$this->evaluating($data)['power'].'</p><p>Sila nepriateľského vojska:'.$this->evaluating($data)['power2'].'</p>');
			}
			redirect('battle');
		}
		$this->battle_mode();
	}

	function soldiers(){
		$this->main_model->return_count_soldiers();
	}

	function troops(){
		$this->main_model->returnTroops();
	}

	function return_weapons(){
		$this->main_model->return_weapons();
	}