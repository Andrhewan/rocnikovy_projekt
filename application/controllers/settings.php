<?php if(!defined("BASEPATH")) exit("No direct script acces allowed");

class Settings extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('settings_model');
		$this->load->helper('language');
		$this->lang->load('game');
	}

	function setting() {
		$this->form_validation->set_rules('nation', 'Národ', 'required');
		$this->form_validation->set_error_delimiters('<p class="error">','</p>');
		if($this->form_validation->run()) {
			echo 'ahoj';
		}
		$this->template->set('title','Nastavenia');
		$this->template->view('settings_view');
	}
}

?>