<?php if(!defined("BASEPATH")) exit("No direct script acces allowed");

class Main_model extends CI_Model {
	function selectPlayerData(){
		$id = $this->session->all_userdata()['id'];
		$this->db->where('id', $id);
		$select = $this->db->get("player_data");
		if($select->num_rows()>0){
			return $select->result_array();
		}
		else{
			return FALSE;
		}
	}

	function giveResource($count,$id){
		$this->db->where('id',$id);
		$data = array(
			'wood'=>$count,
			'iron'=>$count,
			'stone'=>$count,
			'gold' => $count,
			'earth' => $count,
			);
		return $this->db->update('resources_data',$data);
	}

	function basicGameSettings($id) {
		$income = 25;
		$this->db->where('id',$id);
		$data = array(
			'wood_income' => $income,
			'stone_income' => $income,
			'gold_income' => $income,
			'iron_income' => $income,
			'earth_income' => $income,
			'storage_capacity' => 3000,
			);
		$this->db->update('base_game_setting',$data);
	}

	function returnBasicGameSettings(){
		$id = $this->session->all_userdata()['id'];
		$this->db->where('id',$id);
		return $this->db->get('base_game_setting')->result_array()[0];
	}

	function selectInfo($table,$column,$column2,$return){
		$this->db->where($column, $column2);
		$select = $this->db->get($table);
		if($select->num_rows()>0){
			return $select->result_array()[0][$return];
		}
		else{
			return FALSE;
		}
	}

	function returnResource($res){
		$id = $this->session->all_userdata()['id'];
		$this->db->where('id',$id);
		$select = $this->db->get('resources_data');
		if($select->num_rows()>0){
			echo $select->result_array()[0][$res];
		}
		else{
			return FALSE;
		}
	}

	function returnResource2(){
		$id = $this->session->all_userdata()['id'];
		$this->db->where('id',$id);
		$select = $this->db->get('resources_data');
		if($select->num_rows()>0){
			return  $select->result_array()[0];
		}
		else{
			return FALSE;
		}
	}


	function giveExpPlayer(){
		$this->db->order_by('exp','desc');
		$select = $this->db->get('experience');
		return $select->result_array();
	}

	function returnPlayer(){
		$select = $this->db->get('user_data');
		return $select->result_array();
	}

	function showBuildings(){
		$select = $this->db->get('buildings');
		return $select->result_array();
	}

	function showTroops(){
		$select = $this->db->get('troops');
		return $select->result_array();
	}

	function returnTroops(){
		$id = $this->session->all_userdata()['id'];
		$this->db->where('id',$id);
		$select = $this->db->get('player_units');
		echo json_encode($select->result_array()[0]);
	}

	function return_weapons(){
		$id = $this->session->all_userdata()['id'];
		$this->db->where('id',$id);
		$select = $this->db->get('armory');
		echo json_encode($select->result_array()[0]);
	}


	// aumotatic updatig of resoureces

	function return_all_id(){
		$this->db->select('id');
		$this->db->from('user_data');
		$select = $this->db->get();
		print_r(json_encode($select->result_array()));
	}

	function update_res_all($res,$id){
		$this->db->where('id',$id);
		$select = $this->db->get('resources_data')->result_array()[0];
		$data = array($res=>$select[$res]+1);
		$this->db->where('id',$id);
		return $this->db->update('resources_data',$data);
	}

	// return incoming of players*/
	function return_res_per_hour($id){
		$this->db->where('id',$id);
		print_r(json_encode($this->db->get('base_game_setting')->result_array()));
	}





	//automatic updating of resources
	function update($id){
		$this->db->where('id',$id);
		$select = $this->db->get('resources_data')->result_array()[0];
		print_r($select);
		$data = array('wood'=>$select['wood']+1,
			'stone'=>$select['stone']+1,
			'gold'=>$select['gold']+1,
			'earth'=>$select['earth']+1,
			'iron'=>$select['iron']+1,);
		$this->db->where('id',$id);
		return $this->db->update('resources_data',$data);
	}

	function update_all(){
		$select = $this->db->get('resources_data')->result_array();
		foreach($select as $user):
			$this->update($user['id']);
		endforeach;
	}

	function createBuildPlace(){
		$id = $this->session->all_userdata()['id'];
		$this->db->where('id',$id);
		$select = $this->db->get('buildplace')->result_array()[0];
		$i = -1;
		foreach ($select as $place):
			$i++;
			if($place == ''){
				echo '<div class="emptyPlace pointer buildPlace" id="place'.$i.'"" data-placeid="'.$i.'" data-buildingname="Voľné stavebné políčko" title="Voľné stavebné políčko"></div>';
			}
			else{
				if($place == 'Nora'){
					echo '<div class="burrow pointer buildPlace" id="place'.$i.'"" data-placeid="'.$i.'" data-buildingname="'.$place.'" title="'.$place.'"></div>';
				}
				else if($place == 'Kasárne'){
					echo '<div class="barrack pointer buildPlace" id="place'.$i.'"" data-placeid="'.$i.'" data-buildingname="'.$place.'" title="'.$place.'"></div>';
				}
				else if($place == 'Hlavná budova'){
					echo '<div class="townhall pointer buildPlace" id="place'.$i.'"" data-placeid="'.$i.'" data-buildingname="'.$place.'" title="'.$place.'"></div>';
				}
				else if($place == 'Sklad'){
					echo '<div class="storage pointer buildPlace" id="place'.$i.'"" data-placeid="'.$i.'" data-buildingName="'.$place.'" title="'.$place.'"></div>';
				}
				else if($place == 'Dielne'){
					echo '<div class="armory pointer buildPlace" id="place'.$i.'"" data-placeid="'.$i.'" data-buildingname="'.$place.'" title="'.$place.'"></div>';
				}
				else if($place == 'Zbrojnica'){
					echo '<div class="armory2 pointer buildPlace" id="place'.$i.'"" data-placeid="'.$i.'" data-buildingname="'.$place.'" title="'.$place.'"></div>';
				}
			}
		endforeach;
	}

	function createBuildLandPlace(){
		$id = $this->session->all_userdata()['id'];
		$this->db->where('id',$id);
		$select = $this->db->get('landbuildplace')->result_array()[0];
		$i = -1;
		foreach ($select as $place):
			$i++;
			if($place == ''){
				echo '<div class="emptyPlace pointer buildLandPlace" id="placeLand'.$i.'"" data-placeid="'.$i.'" title="Voľné stavebné políčko"></div>';
			}
			else{
				if($place == 'Nora'){
					echo '<div class="burrow pointer buildLandPlace" id="placeLand'.$i.'"" data-placeid="'.$i.'" title="'.$place.'"></div>';
				}
				else if($place == 'Kasárne'){
					echo '<div class="barrack pointer buildLandPlace" id="placeLand'.$i.'"" data-placeid="'.$i.'" title="'.$place.'"></div>';
				}
				else if($place == 'Hlavná budova'){
					echo '<div class="townhall pointer buildLandPlace" id="placeLand'.$i.'"" data-placeid="'.$i.'" title="'.$place.'"></div>';
				}
				else if($place == 'Sklad'){
					echo '<div class="storage pointer buildLandPlace" id="placeLand'.$i.'"" data-placeid="'.$i.'" title="'.$place.'"></div>';
				}
				else if($place == 'Dielne'){
					echo '<div class="armory pointer buildLandPlace" id="placeLand'.$i.'"" data-placeid="'.$i.'" title="'.$place.'"></div>';
				}
				else if($place == 'Zbrojnica'){
					echo '<div class="armory2 pointer buildPlace" id="placeLand'.$i.'"" data-placeid="'.$i.'" title="'.$place.'"></div>';
				}
			}
		endforeach;
	}

	function buildBuilding($building,$wood,$stone,$gold,$iron,$earth,$place,$score,$exp){
		$id = $this->session->all_userdata()['id'];
		$this->db->where('id',$id);
		$data = array(
			'wood'=>$wood,
			'stone'=>$stone,
			'gold'=>$gold,
			'iron'=>$iron,
			'earth'=>$earth,
		);
		
		$this->db->update('resources_data',$data);
		$data2 = array(
			$place => $building,
		);
		$this->db->where('id',$id);
		$this->db->update('buildplace',$data2);

		$this->db->where('id',$id);
		$select = $this->db->get('experience')->result_array()[0];

		$data3 = array(
			'exp' => $select['exp'] + $exp,
			'score' => $select['score'] + $score,
			);

		$this->db->where('id',$id);
		$this->db->update('experience',$data3);
	}

	function verify($building,$place){
		$id = $this->session->all_userdata()['id'];
		$this->db->where('id',$id);
		$select1 = $this->db->get('resources_data')->result_array()[0];
		$this->db->where('building',$building);
		$select2 = $this->db->get('buildings')->result_array()[0];
		$wood = $select1['wood'] - $select2['wood'];
		$stone = $select1['stone'] - $select2['stone'];
		$gold = $select1['gold'] - $select2['gold'];
		$iron = $select1['iron'] - $select2['iron'];
		$earth = $select1['earth'] - $select2['earth'];
		$exp = $select2['exps'];
		$score = $select2['score'];

		if($wood >= 0 && $stone >= 0 && $gold >= 0 && $iron >= 0 && $earth >= 0) {
			$this->buildBuilding($building,$wood,$stone,$gold,$iron,$earth,$place,$score,$exp);
			//echo true;
		}
		else {
			//echo false;
			$this->session->set_flashdata('error_build','<p class="error">Nedostatok surovín!</p>');
		}
	}

	function recruitUnit($unit,$bow,$spear,$sword,$sling,$la,$ha,$gold,$count) {
		$data  = array(
			'bows' => $bow,
			'spears' => $spear,
			'swords' => $sword,
			'light_armours' => $la,
			'heavy_armours' => $ha,
			'slings' => $sling
			);
		$id = $this->session->all_userdata()['id'];
		$this->db->where('id',$id);
		$this->db->update('armory',$data);

		$data2 = array(
			'gold' => $gold,
			);

		$this->db->where('id',$id);
		$this->db->update('resources_data',$data2);

		$this->db->where('id',$id);
		$units = $this->db->get('player_units')->result_array()[0];

		switch($unit){
			case 'Lukostrelec':
				$data3 = array(
				'archers' => $units['archers'] + $count,
				);
				break;
			case 'Bojovník s prakom':
				$data3 = array(
				'slingers' => $units['slingers'] + $count,
				);
				break;
			case 'Kopijník':
				$data3 = array(
				'spearmen' => $units['spearmen'] + $count,
				);
				break;
			case 'Ťažkoodenec':
				$data3 = array(
				'swordmen' => $units['swordmen'] + $count,
				);
				break;
		}

		$this->db->where('id',$id);
		$this->db->update('player_units',$data3);
	}

	function verifyUnit($unit,$count){
		$this->db->where('post',$unit);
		$select1 = $this->db->get('troops')->result_array()[0];
		$id = $this->session->all_userdata()['id'];
		$this->db->where('id',$id);
		$select2 = $this->db->get('armory')->result_array()[0];

		$id = $this->session->all_userdata()['id'];
		$this->db->where('id',$id);
		$select3 = $this->db->get('resources_data')->result_array()[0];

		$bow = $select2['bows'] - $select1['bow']*$count;
		$spear = $select2['spears'] - $select1['spear']*$count;
		$sword = $select2['swords'] - $select1['sword']*$count;
		$sling = $select2['slings'] - $select1['sling']*$count;
		$la = $select2['light_armours'] - $select1['light_armour']*$count;
		$ha = $select2['heavy_armours'] - $select1['heavy_armour']*$count;
		$gold = $select3['gold'] - $select1['gold']*$count;
		echo $bow,'A',$spear,'A',$sword,'A',$sling,'A',$la,'A',$ha,'A',$gold;
		if($bow >= 0 && $spear >= 0 && $sword >= 0 && $sling >= 0 && $la >= 0 && $ha >= 0 && $gold >= 0) {
			$this->recruitUnit($select1['unit'],$bow,$spear,$sword,$sling,$la,$ha,$gold,$count);
		}
	}

	function returnHelp($keyword) {
		$this->db->where('keyword',$keyword);
		print_r( $this->db->get('helptablestrings')->result_array()[0]['string']);
	}

	function incStorageCapacity($inc) {
		$id = $this->session->all_userdata()['id'];
		$this->db->where('id',$id);
		$cap = 2000;

		$id = $this->session->all_userdata()['id'];
		$this->db->where('id',$id);
		$count = 1;
		$select = $this->db->get('buildplace')->result_array()[0];
		
		foreach ($select as $s):
			if($s == 'Sklad') {
				$count++;
			}
		endforeach;

		$incCap = $cap + ($count*$inc);
		$data = array(
			'storage_capacity' => $incCap,
			);
		$id = $this->session->all_userdata()['id'];
		$this->db->where('id',$id);
		$this->db->update('base_game_setting',$data);
	}
	function returnUpdate($select){
		$id = $this->session->all_userdata()['id'];
		$this->db->where('id',$id);
		$this->db->get('base_game_setting')->result_array()[0][$select];
	}

	function verify_builded($building){
		$id = $this->session->all_userdata()['id'];
		$this->db->where('id',$id);
		$select = $this->db->get('buildplace')->result_array();
		if (in_array($building,$select[0])) {
			return true;
		}
		return false;
	}

	function return_count_soldiers(){
		$id = $this->session->all_userdata()['id'];
		$this->db->where('id',$id);
		$select = $this->db->get('player_units')->result_array()[0];
		echo array_sum($select)-$id;
	}
}