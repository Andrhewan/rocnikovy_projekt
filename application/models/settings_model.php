<?php if(!defined("BASEPATH")) exit("No direct script acces allowed");

class Settings_model extends CI_Model {
	
	function select_nation($charnat=''){
		$this->db->where('charnat',$charnat);
		$select = $this->db->get('nations');
		if($select->result_array()>0){
			return $select->result_array();
		}
		else{
			return FALSE;
		}
	}


	function checkSetting($column){
		$id = $this->session->all_userdata()['id'];
		$this->db->select($column);
		$this->db->where('id', $id);
		$select = $this->db->get("player_data");
		if($select->num_rows()>0){
			//print_r($select->result_array()[0][$column]);
			return $select->result_array()[0][$column];
		}
		else{
			return FALSE;
		}
	}

	function select_genus($nat=''){
		$this->db->where('id_nation',$nat);
		$select = $this->db->get('genus_information');
		if($select->result_array()>0){
			//print_r($select->result_array());
			return $select->result_array();
		}
		else{
			return FALSE;
		}
	}


	function saveSetting($column,$value){
		$id = $this->session->all_userdata()['id'];
		$data = array(
			$column=>$value,
			);
		$this->db->where('id',$id);
		return $this->db->update('player_data', $data);
	}

}