<?php if(!defined("BASEPATH")) exit("No direct script acces allowed");

class Auth_model extends CI_Model {
	function register() {
		$data = array(
			'username'=>$_POST['registerName'],
			'userpassword'=>sha1($_POST['registerPassword']),
			);
		return $this->db->insert('user_data', $data);
	}

	function check() {
		$select = $this->db->where('username',$_POST['loginName'])
						  ->where('userpassword',sha1($_POST['loginPassword']))
						  ->get('user_data');
		return $select->num_rows();
		
	}

	function getUserData($userName){
		$select = $this->db->where('username',$userName)
						   ->limit(1)
						   ->get('user_data');
		if ($select->num_rows() > 0) {
			return $select->row_array();
		}
		else{
			return FALSE;
		}
	}

	function checkNewUser(){
		$select = $this->db->where('username',$_POST['registerName'])
						   ->get('user_data');
		echo $select;
		return $select->num_rows();
	}

	function setuID($table,$id){
		$query = $this->db->insert_string($table,array('id'=>$id));
		$this->db->query($query);
	}

	function update_password(){
		$id = $this->session->all_userdata()['id'];
		$this->db->where('id',$id);
		$data = array(
			'userpassword'=>sha1($_POST['newPassword']),
			);
		return $this->db->update('user_data', $data);
	}

}