<?php
$lang['title']  				= " | Vojna trollow a goblinov";

// auth
$lang['login']					= "Prihlásenie";
$lang['register']				= "Registrácia";
$lang['notLogin']				= "Už si zaregistrovaný? Tak sa prihlás!";
$lang['notRegistration']		= "Ešte si sa neregistroval? Zaregistruj sa!";
$lang['registerUp']				= "Zaregistruj sa";
$lang['loginUp']				= "Prihlás sa";
$lang['pName'] 					= "Meno hráča:";
$lang['pPass'] 					= "Heslo hráča:";
$lang['notRegister']			= "Nie si zaregistrovaný!";
$lang['isRegistered']			= "Užívateľ s menom %s je už registrovaný!";

// resourceBar label
$lang['hourIncoming']			= "Príjem za hodinu";
$lang['storageCapacity']		= "Kapacita skladu";
$lang['storedRes'] 				= "%s v sklade";
$lang['wood']					= "Drevo";
$lang['stone']					= "Kameň";
$lang['gold']					= "Zlato";
$lang['iron']					= "Železo";
$lang['earth'] 					= "Hlina";

// actionBar label
$lang['showWorld']				= "Náhľad sveta";
$lang['showTown']				= "Náhľad mesta";
$lang['diplomacy']				= "Diplomacia";
$lang['army']					= "Armáda";
$lang['help']					= "Pomoc";
$lang['scoreTable']				= "Hodnotenie hráča";
$lang['builder']				= "Prehľad budov";
$lang['research']				= "Výskum";

// button label
$lang['logout']					= "Odhlásenie";
$lang['close']					= "Zavrieť aktuálny výber";
$lang['closeWindow']			= "Zavrieť okno";

// scoreTable
$lang['playersScoreHeadline']	= "Prehľad hodnotenia hráčov";
$lang['player']					= "Hráč";
$lang['score']					= "Body";

// helpTable
$lang['changePassword']			= "Zmena hesla";
$lang['errorOccure']			= "Nastala chyba!";
$lang['okChangePass']	 		= "Úspešne ste zmenili heslo!";
$lang['resources']				= "Suroviny";
$lang['resourcesInfo']			= "Info o surovinách";
$lang['buildings']				= "Budovy";
$lang['army']					= "Armáda";
$lang['build']					= "Výstavba";
$lang['storage']				= "Sklad";
$lang['armory']					= "Zbrojenie";
$lang['population']				= "Populácia";
$lang['quest']					= "Úlohy a tipy";

$lang['battle_h']				= "Vojnová kalkulačka";
$lang['eval_b']					= "Vyhodnoť bitku";
	
?>