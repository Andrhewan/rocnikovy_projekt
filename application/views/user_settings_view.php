<h1 class='h'>
	Nastavenie hesla
</h1>
<div class='setting_box'>
	<div id='errors'>
		<?php 
			echo validation_errors();
		?>
		<?= $this->session->flashdata('succ_message'); ?>
		<?= $this->session->flashdata('error_message'); ?>
	</div>
		<?php
			echo form_open('setuser');
			echo form_label('Nové heslo:','newPassword');
			$data = array(
					'name' => 'newPassword',
					'value' => set_value('newPassword'),
					'id' => 'newPassword',
				);
			echo form_password($data);
			echo '<br>';
			$data = array(
					'name' => 'newPassword2',
					'value' => set_value('newPassword2'),
					'id' => 'newPassword2',
				);
			echo form_label('Overenie nového hesla:','newPassword2');
			echo form_password($data);
			echo '<br>';
			echo form_submit("submit", "Potvrďte zmenu");
			echo form_close();
		?>
	<a href="<?= base_url() ?>townmode" class='center'>
    	Späť do hry
 	</a>
</div>