<h1 class='h'>
	<?= lang('battle_h'); ?>
</h1>
<div class="setting_box">
	<div id='errors'>
		<?php 
			echo validation_errors();
		?>
		<?= $this->session->flashdata('win_message'); ?>
		<?= $this->session->flashdata('loose_message'); ?>
	</div>
	<?php echo form_open('battle_eval');?>
	<table>
		<tr>
			<th>
				Tvoje vojsko
			</th>
			<th>
			</th>
			<th>
				Nepriateľské vojsko
			</th>
			<th>
			</th>
		</tr>
		<tr>
			<td>
				Lukostrelec
			</td>
			<td>
				<?php 
				$data = array(
					'name' => 'archer_val',
					'value' => set_value('archer_val',0),
					'id' => 'archer_val',
				);
				echo form_input($data);
				?>
			</td>
			<td>
				Lukostrelec
			</td>
			<td>
				<?php 
				$data = array(
					'name' => 'archer_val1',
					'value' => set_value('archer_val1',0),
					'id' => 'archer_val1',
				);
				echo form_input($data);
				?>
			</td>
		<tr>
			<tr>
			<td>
				Strelec s prakom
			</td>
			<td>
				<?php 
				$data = array(
					'name' => 'sling_val',
					'value' => set_value('sling_val',0),
					'id' => 'sling_val',
				);
				echo form_input($data);
				?>
			</td>
			<td>
				Strelec s prakom
			</td>
			<td>
				<?php 
				$data = array(
					'name' => 'sling_val1',
					'value' => set_value('sling_val1',0),
					'id' => 'sling_val1',
				);
				echo form_input($data);
				?>
			</td>
		<tr>
			<tr>
			<td>
				Kopijník
			</td>
			<td>
				<?php 
				$data = array(
					'name' => 'spear_val',
					'value' => set_value('spear_val',0),
					'id' => 'spear_val',
				);
				echo form_input($data);
				?>
			</td>
			<td>
				Kopijník
			</td>
			<td>
				<?php 
				$data = array(
					'name' => 'spear_val1',
					'value' => set_value('spear_val1',0),
					'id' => 'spear_val1',
				);
				echo form_input($data);
				?>
			</td>
		<tr>
			<tr>
			<td>
				Ťažkoodenec
			</td>
			<td>
				<?php 
				$data = array(
					'name' => 'sword_val',
					'value' => set_value('sword_val',0),
					'id' => 'sword_val',
				);
				echo form_input($data);
				?>
			</td>
			<td>
				Ťažkoodenec
			</td>
			<td>
				<?php 
				$data = array(
					'name' => 'sword_val1',
					'value' => set_value('sword_val1',0),
					'id' => 'sword_val1',
				);
				echo form_input($data);
				?>
			</td>
		<tr>
	</table>
	<?php 
	echo form_submit("eval_battle", lang('eval_b'));
	echo form_close();
	?>
	<a href="<?= base_url();?>townmode">Spat do hry</a>
</div>