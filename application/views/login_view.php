<h1 class='h'>
	<?= lang('login'); ?>
</h1>
<div class="setting_box">
	<div id='errors'>
		<?php 
			echo validation_errors();
		?>
	</div>
	<?= $this->session->flashdata('register_error_message'); ?>

	<?php
		echo form_open('login');
		echo form_label(lang('pName'),'loginName');
		$data = array(
				'name' => 'loginName',
				'value' => set_value('loginName'),
				'id' => 'loginName',
			);
		echo form_input($data);
		echo '<br>';
		echo form_label(lang('pPass'),'loginPassword');
		$data = array(
				'name' => 'loginPassword',
				'value' => set_value('loginPassword'),
				'id' => 'loginPassword',
			);
		echo form_password($data);
		echo '<br>';
		echo form_submit("submit", lang('loginUp'));
		echo form_close();
	?>
	<a href="<?= base_url('register') ?>">
		<?= lang('notRegistration'); ?>
	</a>
</div>