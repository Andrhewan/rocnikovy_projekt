<h1>
	Pomoc
</h1>
<h2>
	Modes
</h2>
<p>
	The game has three modes (town, land, world).
After log in to game and creating the player profile, the game is in “town” mode. The
town is showed in this mode.
When the game is changed to “land” mode, the nearest land around the town and icon of
the town is visible.
The last mode, “world”, is a map of world where player is.
</p>

<h2>
	Resources
</h2>
<p>
	The resources are given automatically after registration. The basic goods are given
constantly for all players. When players is doing some action, for example, building or
recruiting, the resources are changed by this action.
The all resources can be seen in the building Storage. On the top of a side are visible just
basic resources.
The resources will be given by the system setting in time interval too.
The resources includes the weapons too.
</p>

<h2>
	Buildings
</h2>
<p>
	The primary action in the town is building buildings. There are fixed places for building
in town. These buildings can improve by level.
</p>

<h2>
	Recruiting
</h2>
<p>
	When building Barrack is built in the town, the soldiers can be recruiting. Various types
of soldiers are depended from their requirements.
</p>

<a href="<?= base_url();?>townmode">Spat do hry</a>