<?php 
	echo form_open('login');?>
	<button type="submit" id='logout' title='<?= lang('logout');?>'></button>
<?php 
	echo form_close();
?>
<div id="experienceBar">
	<?php 
	foreach ($exp as $ep):
		if ($ep['id'] == $player['id']){
			?>
			<div id="showExp" title='<?php echo $ep['exp']; ?> /1000 skúseností'>
  				<?php $expWidth = (290/1000)* $ep['exp'];?>
    			<img src="<?= base_url('images/exp.jpg') ?>" alt="<?php echo $ep['exp']; ?> /1000 skúseností" title="<?php echo  $ep['exp']; ?> /1000 skúseností" width=<?php echo $expWidth.'px';?> height="5px">
			</div>
			<?php
		} 
	endforeach;
	?>
</div>
<?php
	$nation = $data;
	$genus = $data2;
	$name = $this->session->all_userdata()['username'];
	$capital = $data3;
	echo '<div id="profile"><p>'.$name.' ('.$genus.' - '.$nation.') <br> '.$capital.'</p></div>'
?>

<div id="resourcesBar">
  <?php 
    $capacity = $bs['storage_capacity'];
  ?>
  <ul>
    <li>
      <div id="wood" title="<?= lang('wood') ?>"></div>
      <span id="resource" title="<?= sprintf(lang('storedRes'),lang('wood')); ?>" class='wood'>
      	<?= $res['wood']; ?>
      </span>
      <span id="income" title="<?= lang('hourIncoming'); ?>">
        <?= '+'.$bs['wood_income']; ?>
      </span>
      <span id="capacity" title="<?= lang('storageCapacity'); ?>">
        <?= $capacity; ?>
      </span>
    </li>
    <li>
      <div id="stone" title="<?= lang('stone'); ?>"></div>
      <span id="resource" title="<?= sprintf(lang('storedRes'),lang('stone')); ?>" class='stone'>
      	<?= $res['stone']; ?>
      </span>
      <span id="income" title="<?= lang('hourIncoming'); ?>">
        <?= '+'.$bs['stone_income']; ?>
      </span>
      <span id="capacity" title="<?= lang('storageCapacity'); ?>">
        <?= $capacity; ?>
      </span>
    </li>
    <li>
      <div id="gold" title="<?= lang('gold'); ?>"></div>
      <span id="resource" title="<?= sprintf(lang('storedRes'),lang('gold')); ?>" class='gold'>
      	<?= $res['gold']; ?>
      </span>
      <span id="income" title="<?= lang('hourIncoming'); ?>">
        <?= '+'.$bs['gold_income']; ?>
      </span>
      <span id="capacity" title="<?= lang('storageCapacity'); ?>">
        <?= $capacity; ?>
      </span> 
    </li>
    <li>
      <div id="iron" title="<?= lang('iron'); ?>"></div>
      <span id="resource" title="<?= sprintf(lang('storedRes'),lang('iron')); ?>" class='iron'>
      	<?= $res['iron']; ?>
      </span>
      <span id="income" title="<?= lang('hourIncoming'); ?>">
        <?= '+'.$bs['iron_income']; ?>
      </span>
      <span id="capacity" title="<?= lang('storageCapacity'); ?>">
        <?= $capacity; ?>
      </span>
    </li>
    <li>
      <div id="earth" title="<?= lang('earth'); ?>"></div>
      <span id="resource" title="<?= sprintf(lang('storedRes'),lang('earth')); ?>" class='earth'>
      	<?= $res['earth']; ?>
      </span>
      <span id="income" title="<?= lang('hourIncoming'); ?>">
        <?= '+'.$bs['earth_income']; ?>
      </span>
      <span id="capacity" title="<?= lang('storageCapacity'); ?>">
        <?= $capacity; ?>
      </span>
    </li>
  </ul>
</div>

<div id="actionBar">
  <ul>
    <li id="map">
      <a href="<?= base_url('worldmode')?>" title="<?= lang('showWorld'); ?>"></a>
    </li>
    <li id="town">
      <a href="<?= base_url('townmode')?>" title="<?= lang('showTown'); ?>"></a>
    </li>
    <li id="diplomacy">
      <a href="#diplomacy" title="<?= lang('diplomacy'); ?>"></a>
    </li>
    <li id="stoarm">
      <a href="#armory" title="Sklad/Zbrojnica"></a>
    </li>
    <li id="army" title="<?= lang('army'); ?>">
    </li>
    <li id="research">
      <a href="#research" title="<?= lang('research'); ?>"></a>
    </li>
    <li id="statistic" title="<?= lang('scoreTable'); ?>">
    </li>
    <li id="help" title="<?= lang('help'); ?>">
    </li>
</div>

<div id="scoreTable">
	<h1>
		<?= lang('playersScoreHeadline'); ?>
	</h1>
	<span title='<?= lang('closeWindow'); ?>'>
		X
	</span>
	<table>
		<tr>
			<th>
				<?= lang('player'); ?>
			</th>
			<th>
				<?= lang('score'); ?>
			</th>
		</tr>
	<?php 
	foreach ($exp as $ep):
		foreach($players as $pl):
			if($pl['id'] == $ep['id']){
				if($pl['username'] == $player['username']){
					echo '<tr class="actual">';
					echo '<td>'.$pl['username'].'</td><td>'.$ep['score'].'</td>';
					echo '</tr>';
				}
				else{
					echo '<tr>';
					echo '<td>'.$pl['username'].'</td><td>'.$ep['score'].'</td>';
					echo '</tr>';
				}
			}
		endforeach;
	endforeach;
	?>
	</table>
</div>

<div id="helpTable">
  <h1>
    <?= lang('help'); ?>
  </h1>
  <span title='<?= lang('closeWindow'); ?>'>
    X
  </span>
  <ul>
    <li>
      <a href="<?= base_url('setuser') ?>">
        <?= lang('changePassword'); ?>
      </a>
    </li>
    <li>
      <a href="<?= base_url('help') ?>">
        <?= lang('help'); ?>
      </a>
    </li>
    <li data-resources='resources'>
      <?= lang('resources'); ?>
    </li>
    <li data-resources='buildings'>
      <?= lang('buildings'); ?>
    </li>
     <li data-resources='build'>
      <?= lang('build'); ?>
    </li>
    <li data-resources='army'>
      <?= lang('army'); ?>
    </li>
    <li data-resources='storage'>
      <?= lang('storage'); ?>
    </li>
    <li data-resources='armory'>
      <?= lang('armory'); ?>
    </li>
    <li data-resources='population'>
      <?= lang('population'); ?>
    </li>
    <li data-resources='quest'>
      <?= lang('quest'); ?>
    </li>
    <li>
      <a href="<?= base_url('battle') ?>">
        <?= lang('battle_h'); ?>
      </a>
    </li>
  </ul>
  <div id="containHelpTable">
  </div>
</div>

<div id="bottomBar">
  <span title='<?= lang('close'); ?>'>
  </span>
  <h1>
  </h1>
  <div id='troops'>
    <?php 
    if ($this->load->main_model->verify_builded('Kasárne')){
      foreach($troops as $troop):
        echo '<div id="'.substr($troop["post"],3).'" class="button pointer" title="'.$troop['unit'].'">';
        echo '</div>';
      endforeach;
    }
    else{
      echo '<p class="loose2">Nie je postavená kasáreň</p>';
    }
    ?>
  </div>
  <div id='buildings'>
    <?php
    if ($this->load->main_model->verify_builded('Hlavná budova')){
      foreach($buildings as $building):
        if($building['level'] == 1){
          echo '<div id="'.substr($building["post"],5).'" class="button pointer" title="'.$building['building'].'">';
          echo '</div>';
        }
      endforeach;
    }
    else{
      foreach($buildings as $building):
        if($building['level'] == 1 && $building['building'] == 'Hlavná budova'){
          echo '<div id="'.substr($building["post"],5).'" class="button pointer" title="'.$building['building'].'">';
          echo '</div>';
        }
      endforeach;
    }
    ?>
  </div>
</div>

<div id="infoBar">
  <span title="<?= lang('close'); ?>" class="pointer">
  </span>
  <div id="infoBuilding">
      <?php 
      foreach ($buildings as $building):
        if($building['level'] == 1){
          echo '<div id="info'.substr($building["post"],5).'">';
          echo '<h1>'.$building['building'].'</h1>';
          echo $building['description']; ?>
          <table>
            <tr><td><div id="wood" title="Drevo"></div></td><td><?php echo $building['wood']; ?></td>
              <td><div id="gold" title="Zlato"></div></td><td><?php echo $building['gold']; ?></td></tr>
            <tr><td><div id="iron" title="Železo"></div></td><td><?php echo $building['iron']; ?></td>
              <td><div id="stone" title="Kameň"></div></td><td><?php echo $building['stone']; ?></td></tr>
            <tr><td><div id="time" title="Čas"></div></td><td><?php echo $building['time']; ?></td></tr>
          </table>
          <?php
          echo form_open('townmode');
          ?>
          <button type="submit" id='<?php echo $building['post']; ?>' name='<?php echo $building['post']; ?>'>Postav!</button>
          <?php
          $this->session->flashdata('exist_error_message');
          echo form_close();
          echo '</div>';
        }
      endforeach;
      ?>
  </div>
  <div id="infoUnit">
    <?php 
      foreach ($troops as $troop):
        echo '<div id="info'.substr($troop["post"],3).'">';
        echo '<h1>'.$troop['unit'].'</h1>';
        echo $troop['description']; ?>
        <table>
            <tr><td><div id="sling" title="Prak"></div></td><td><?php echo $troop['sling']; ?></td>
              <td><div id="gold" title="Zlato"></div></td><td><?php echo $troop['gold']; ?></td></tr>
            <tr><td><div id="sword" title="Meč"></div></td><td><?php echo $troop['sword']; ?></td>
              <td><div id="spear" title="Kopija"></div></td><td><?php echo $troop['spear']; ?></td></tr>
            <tr><td><div id="time" title="Čas"></div></td><td><?php echo $troop['time']; ?></td></tr>
          </table>
        <?php
        echo form_open('main/recruit/'.$troop['post']);
        $data = array(
          'name' => $troop['post'].'_val',
          'value' => set_value($troop['post'].'_val',0),
          'id' => $troop['post'].'_val',
        );
        echo form_input($data);
        echo form_submit($troop['post'], "Najmi!");
        ?>
          <?php
          $this->session->flashdata('exist_error_message');
          echo form_close();
        echo '</div>';
      endforeach;
      ?>
  </div>
</div>
<div id="places">
</div>
<div id='backgroundLandMode'>
</div>