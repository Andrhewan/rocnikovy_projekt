<div id="setting">
	<h1>
		Nastavenia
	</h1>

	<div id='errors'>
		<?php 
			echo validation_errors();
		?>
	</div>

	<?php
		echo form_open('settings/setting');
		echo form_label('Narod:','nation');
		$data = array(
				'name' => 'nation',
				'value' => set_value('nation'),
				'id' => 'nation',
			);
		echo form_input($data);
		echo '<br>';
		echo form_submit("submit", "Potvrd");
		echo form_close();
	?>
</div>