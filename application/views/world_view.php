<h1>
	Svet
</h1>
<a href='<?= base_url();?>landmode'>
	OKOLIE
</a>
<?php 
	echo form_open('login');?>
	<button type="submit" id='logout' title='Odhlásenie'></button>
<?php 
	echo form_close();
?>
<div id="experienceBar">
	<?php 
	foreach ($exp as $ep):
		if ($ep['id'] == $player['id']){
			?>
			<div id="showExp" title='<?php echo $ep['exp']; ?> /1000 skúseností'>
  				<?php $expWidth = (290/1000)* $ep['exp'];?>
    			<img src="<?= base_url() ?>images/exp.jpg" alt="<?php echo $ep['exp']; ?> /1000 skúseností" title="<?php echo  $ep['exp']; ?> /1000 skúseností" width=<?php echo $expWidth.'px';?> height="5px">
			</div>
			<?php
		} 
	endforeach;
	?>
</div>
<?php
	$nation = $data;
	$genus = $data2;
	$name = $this->session->all_userdata()['username'];
	$capital = $data3;
	echo '<div id="profile"><p>'.$name.' ('.$genus.' - '.$nation.') <br> '.$capital.'</p></div>'
?>

<div id="resourcesBar">
  <?php 
    $capacity = 2000;
  ?>
  <ul>
    <li>
      <div id="wood" title="Drevo"></div>
      <span id="resource" title="Počet dreva v sklade" class='wood'>
      	<?php
          echo $res['wood'];
        ?>
      </span>
      <span id="income" title="Príjem za hodinu">
        +25
      </span>
      <span id="capacity" title="Kapacita skladu">
        <?php 
        echo $capacity;
        ?>
      </span>
    </li>
    <li>
      <div id="stone" title="Kameň"></div>
      <span id="resource" title="Počet kameňa v sklade" class='stone'>
      	<?php
      		echo $res['stone'];
      	?>
      </span>
      <span id="income" title="Príjem za hodinu">
        +25
      </span>
      <span id="capacity" title="Kapacita skladu">
        <?php 
        echo $capacity;
        ?>
      </span>
    </li>
    <li>
      <div id="gold" title="Zlato"></div>
      <span id="resource" title="Počet zlata v sklade" class='gold'>
      	<?php
      		echo $res['gold'];
      	?>
      </span>
      <span id="income" title="Príjem za hodinu">
        +25
      </span>
      <span id="capacity" title="Kapacita skladu">
        <?php 
        echo $capacity;
        ?>
      </span> 
    </li>
    <li>
      <div id="iron" title="Železo"></div>
      <span id="resource" title="Počet železa v sklade" class='iron'>
      	<?php
      		echo $res['iron'];
      	?>
      </span>
      <span id="income" title="Príjem za hodinu">
        +25
      </span>
      <span id="capacity" title="Kapacita skladu">
        <?php 
        echo $capacity;
        ?>
      </span>
    </li>
    <li>
      <div id="earth" title="hlina"></div>
      <span id="resource" title="Počet hliny v sklade" class='earth'>
      	<?php
      		echo $res['earth'];
      	?>
      </span>
      <span id="income" title="Príjem za hodinu">
        +25
      </span>
      <span id="capacity" title="Kapacita skladu">
        <?php 
        echo $capacity;
        ?>
      </span>
    </li>
  </ul>
</div>

<div id="actionBar">
  <ul>
    <li id="map">
      <a href="<?= base_url()?>worldmode" title="Náhľad sveta"></a>
    </li>
    <li id="town">
      <a href="<?= base_url()?>townmode" title="Náhľad mesta"></a>
    </li>
    <li id="diplomacy">
      <a href="#diplomacy" title="Diplomacia"></a>
    </li>
    <li id="stoarm">
      <a href="#armory" title="Sklad/Zbrojnica"></a>
    </li>
    <li id="army" title="Armáda">
    </li>
    <li id="research">
      <a href="#research" title="Výskum"></a>
    </li>
    <li id="statistic">
    </li>
    <li id="help" title="Pomoc">
    </li>
</div>

<div id="scoreTable">
	<h1>
		Prehľad hodnotenia hráčov
	</h1>
	<span title='Zavrieť okno'>
		X
	</span>
	<table>
		<tr>
			<th>
				Hráč
			</th>
			<th>
				Body
			</th>
		</tr>
	<?php 
	foreach ($exp as $ep):
		foreach($players as $pl):
			if($pl['id'] == $ep['id']){
				if($pl['username'] == $player['username']){
					echo '<tr class="actual">';
					echo '<td>'.$pl['username'].'</td><td>'.$ep['exp'].'</td>';
					echo '</tr>';
				}
				else{
					echo '<tr>';
					echo '<td>'.$pl['username'].'</td><td>'.$ep['exp'].'</td>';
					echo '</tr>';
				}
			}
		endforeach;
	endforeach;
	?>
	</table>
</div>

<div id="helpTable">
  <h1>
    Pomoc
  </h1>
  <span title='Zavrieť okno'>
    X
  </span>
  <ul>
    <li>
      <a href="<?= base_url() ?>setuser">
        Zmena hesla
      </a>
    </li>
    <li>
      <a href="<?= base_url() ?>help">
        Pomoc
      </a>
    </li>
  </ul>
</div>

<div id="bottomBar">
  <span title='Zavrieť výber'>
  </span>
  <h1>
  </h1>
  <div id='troops'>
    <?php 
    foreach($troops as $troop):
      echo '<div id="'.substr($troop["post"],3).'" class="button pointer" title="'.$troop['unit'].'">';
      echo '</div>';
    endforeach;
    ?>
  </div>
  <div id='buildings'>
    <?php 
    foreach($buildings as $building):
      if($building['level'] == 1){
        echo '<div id="'.substr($building["post"],5).'" class="button pointer" title="'.$building['building'].'">';
        echo '</div>';
      }
    endforeach;
    ?>
  </div>
</div>

<div id="infoBar">
  <span title="Zavrieť aktuálny výber" class="pointer">
  </span>
  <div id="infoBuilding">
      <?php 
      foreach ($buildings as $building):
        if($building['level'] == 1){
          echo '<div id="info'.substr($building["post"],5).'">';
          echo '<h1>'.$building['building'].'</h1>';
          echo $building['description']; ?>
          <table>
            <tr><td><div id="wood" title="Drevo"></div></td><td><?php echo $building['wood']; ?></td>
              <td><div id="gold" title="Zlato"></div></td><td><?php echo $building['gold']; ?></td></tr>
            <tr><td><div id="iron" title="Železo"></div></td><td><?php echo $building['iron']; ?></td>
              <td><div id="stone" title="Kameň"></div></td><td><?php echo $building['stone']; ?></td></tr>
            <tr><td><div id="time" title="Čas"></div></td><td><?php echo $building['time']; ?></td></tr>
          </table>
          <?php
          echo form_open('townmode');
          ?>
          <button type="submit" id='<?php echo $building['post']; ?>' name='<?php echo $building['post']; ?>'>Postav!</button>
          <?php
          echo form_close();
          echo '</div>';
        }
      endforeach;
      ?>
  </div>
  <div id="infoUnit">
    <?php 
      foreach ($troops as $troop):
        echo '<div id="info'.substr($troop["post"],3).'">';
        echo $troop['description'];
        echo form_open('main');
        echo '</div>';
      endforeach;
      ?>
  </div>
</div>
<div id="bmap">
</div>
