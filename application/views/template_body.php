<!DOCTYPE html>
<html>
	<head>
		<title>
			<?= $title, '| Vojna Trollov a Goblinov'?>
		</title>
		<meta charset='utf-8'>
		<script src="<?= base_url('assets/jquery.min.js') ?>"></script>
		<script src="<?= base_url('assets/jquery-ui.js') ?>"></script>
		<script src="<?= base_url('assets/jquery-1.11.0.min.js') ?>"></script>
		<script src="<?= base_url('assets/scripts.js') ?>"></script>
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/main_styles.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/styles_specifikacia.css') ?>">
	</head>
	<body>

		<?= $content ?>

		<footer>
			<p>
				Created by Vladimir Simko &copy; 2014
			</p>
		</footer>
	</body>
</html>