<h1 class='h'>
	Nastavenia
</h1>

<section class='setting_box'>
	<h1>
		Vyber si oblasť
	</h1>
	<?php
		echo validation_errors();
		echo form_open('settings');

		foreach($genus as $gen):
			$option[$gen['id_gen']] = $gen['genus'];
		endforeach;

		echo form_label('Vyber si kraj:','genus');
		echo form_dropdown('genus',$option);
	?>

	<h1>
		Zadaj názov svojho mesta
	</h1>
	<?php
		$data = array(
				'name' => 'town',
				'id' => 'nametown',
				'value' => set_value('town'),
			);
		echo form_label('Názov mesta','nametown');
		echo form_input($data);
		echo '<br>';
		echo form_submit("submit", "Potvrď nastavania");
		echo form_close();
	?>
</section>