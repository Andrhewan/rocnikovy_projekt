<h1 class='h'>
	<?= lang('register'); ?>
</h1>
<div class="setting_box">
	<div id='errors'>
		<?php 
			echo validation_errors();
		?>
	</div>
	<?= $this->session->flashdata('exist_error_message'); ?>

	<?php
		echo form_open('register');
		echo form_label(lang('pName'),'registerName');
		$data = array(
				'name' => 'registerName',
				'value' => set_value('registerName'),
				'id' => 'registerName',
			);
		echo form_input($data);
		echo '<br>';
		$data = array(
				'name' => 'registerPassword',
				'value' => set_value('registerPassword'),
				'id' => 'registerPassword',
			);
		echo form_label(lang('pPass'),'registerPassword');
		echo form_password($data);
		echo '<br>';
		echo form_submit("submit", lang('registerUp'));
		echo form_close();
	?>
	<a href="<?= base_url('login') ?>">
		<?= lang('notLogin');?>
	</a>
	<br>
	<a href="<?= base_url('rsr') ?>">
		Špecifikácia
	</a>
</div>