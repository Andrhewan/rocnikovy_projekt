<header>
    <h1>
      Ročníkový projekt - Vojna Trollov a Goblinov
    </h1>
    <p>
      <a href="<?= base_url('register') ?>" title="Návrat k projektu">Návrat k projektu</a>
    </p>
  </header>
  
  <section>
    <h1>
      Purpose
    </h1>
    <p>
      The purpose of the project is creating a web-browser game, which will be presented on
the local server. In this game you can create your own account and player profile. The
game is situated to game story where your target is building your settlement, create
soldiers and exploring the nearest area around your settlement. You have to follow the
game story and progress in game. You can see your progress in global table.
    </p>
  </section>
  
  <section>
    <h1>
     Software Requirements Specification
    </h1>
    <a href="RSR_Simko.pdf">Software Requirements Specification</a>
  </section>
  
  <section>
    <h1>
      The game rules
    </h1>
    <p>
      The target is to follow the game story and complete various tasks. The tasks are vital for
further progress in the game, for example, building infrastructure and taking care of the
citizens and so on. Around the town there will be a small piece of land where you can
improve and expand your town. This game will include a few fight scenes with the
opponent nation.
    </p>                                                                                                                                                                      
  </section>
  
  <section>
    <h1>
     About game
    </h1>
    <p>
      When you started the game, you should have to register. However the game is on the
local server, the players register name and register password is satisfactory. This
information will be saved to a new row generated in tables in database.
    </p>
    <p>
      Log in to game follows after the registration. When player is logged in, follows the
games settings, which are important for the game. The settings has got several parts.
    </p>
    <p>
     Selecting of nation and races. Named you town and selection of land. Enter to game. When the game is started, player profile is showed.
    </p>
    <p>
     The game has three modes (town, land, world).
After log in to game and creating the player profile, the game is in “town” mode. The
town is showed in this mode.
When the game is changed to “land” mode, the nearest land around the town and icon of
the town is visible.
The last mode, “world”, is a map of world where player is.
    </p>
    <p>
    The resources are given automatically after registration. The basic goods are given
constantly for all players. When players is doing some action, for example, building or
recruiting, the resources are changed by this action.
    </p>
    <p>
     The primary action in the town is building buildings. There are fixed places for building
in town. These buildings can improve by level.
    </p>
    <p>
     When building Barrack is built in the town, the soldiers can be recruiting. Various types
of soldiers are depended from their requirements.
    </p>
    <p>
    When the game is created after registration, the part of player town is nearest land too. In
this project will be visible just this one land of the player town. The other lands will be
forbidden.
    </p>
    <p>
     In the main table are scores of all players in game. The table is available for all players.
    </p>
  </section>
  <section>
    <h1>
     Who is the game for?
    </h1>
    <p>
     This game is for all persons, whose age is higher then 12 years. However, the game
includes the war themes, it may have a bad influence to younger person.
    </p>
  </section>
  <footer>
    Vytvorené: &copy; Vladimír Šimko | 2013
  </footer> 