<h1 class='h'>
	Nastavenia
</h1>

<section class='setting_box'>
	<h1>
		Vyber si rasu
	</h1>
	<h2>
		Goblini
	</h2>
	<div id='errors'>
		<?php 
			echo validation_errors();
		?>
	</div>
	<?php
		echo form_open('settings');
		echo '<table>';
		foreach ($goblins as $nat):
			echo '<tr>';
			$data = array(
				'name' =>'nation',
				'id' => $nat['id_nation'],
				'value' => $nat['id_nation'],
				);
			echo '<td>'.form_label($nat['nations'],$nat['id_nation']).'</td>';
			echo '<td>'.form_radio($data).'</td>';
			echo '</tr>';
		endforeach;
		echo '</table>';
	?>
	<h2>
		Trollovia
	</h2>
	<?php 
		echo '<table>';
		foreach ($trolls as $nat):
			echo '<tr>';
			$data = array(
				'name' => $nat['nation'],
				'id' => $nat['trolls'],
				'value' => $nat['trolls'],
				);
			echo '<td>'.form_label($nat['trolls'],'x').'</td>';
			echo '<td>'.form_radio($data).'</td>';
			echo '</tr>';
		endforeach;

		echo '</table>';
		echo '<p class="error">Tento národ je zamknutý</p>';

		echo form_submit("submit_nation", "Potvrď národ");
		echo form_close();
	?>
</section>